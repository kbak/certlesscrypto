
\chapter{Background}
\label{chapter:background}
The chapter introduces the field of public-key cryptography and outlines various problems specific to this area. Furthermore, it discusses several methods of guaranteeing authenticity of public-keys and indicates how certificateless cryptography relates to them. Besides problem analysis, it also presents underlying mathematics of elliptic curve cryptography (ECC)\nomenclature{ECC}{Elliptic Curve Cryptography} and pairing-based cryptography, which are crucial for understanding CL-PKE scheme.

\section{Problem Analysis}
Providing authenticity of public keys is one of the hardest problems within public-key cryptography. There are no obvious solutions for this issue, because all of them have some drawbacks and limitations. Fortunately, there are several approaches to the stated problem, and all of them concentrate around one idea: trust. Public-key cryptography is used when two parties are not able to meet and exchange crypto-keys directly; which is almost always the case in computer networks. So, if people do not know each other, how can they know that certain key belongs to the person who claims so? The answer is that, in general case, they cannot know this fact just from looking at the key. There must be some other parties who know the two users and who are trusted by them. This crucial observation leads to several different realizations of the trusted third party idea. 

The two general approaches can be described as decentralized and centralized trusted third parties. The former is somewhat similar to peer-to-peer networks, where every node has equal capabilities and rights. The latter resembles traditional client-server model, where server plays central role and provides services to clients. 

There are universal questions concerning both models of trust. Even though, decentralized trust might seem more fair and reliable, it may be vulnerable to manipulation and can discriminate against new users. On the other hand, centralized trust relies on single points of failure (servers) and might raise doubts whether particular party should be trusted, to what extent, and finally, why. To conclude, one needs to decide which model of trust fits better her needs and should be aware of far-reaching consequences of the decision.

\subsection{Web of Trust}
Web of trust is the most common implementation of decentralized trusted third party. Its infrastructure is very simple, since there are no additional nodes besides users. Users collect public keys of others and certify them if they are sure that particular key belongs to particular person. When two parties do not know each other, they must know other people who belong to trusted chain and can provide a link between the two parties. Of course, every person in the chain must be trusted by the participants.

An example of web of trust can be found in Fig. \ref{fig:weboftrust}. When Alice obtains Bob's key, she can verify that it really belongs to Bob. Alice trusts Isabel and Isabel trusts Bob, therefore Alice can trust Bob. On the other hand, if Alice receives Andrew's key, there is no way in which she can bind Andrew with the key, since there is no one who trusts Andrew, although Andrew trusts Alice, Bob  and Adam.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.8\textwidth]{img/weboftrust}
  \caption{Sample web of trust.}
  \label{fig:weboftrust}
\end{figure}

Due to lack of single certificate authority users are made to collect public-keys and certificates on their own machines. The concept of web of trust is employed by OpenPGP \nomenclature{PGP}{Pretty Good Privacy} systems. This model of trust is very flexible and gives users much freedom when it comes to certification. Every user decides himself whether to accept or reject certain key. OpenPGP was initially meant to protect personal emails, but nowadays it is used to encrypt/sign files and even the whole drives. It is especially popular among more advanced users coming from academia or open source movement. Companies usually prefer centralized models, and for this reason do not incorporate web of trust.

\subsubsection{Advantages}
Decentralization of trust and certificate management facilities make web of trust a very reliable mechanism. It does not contain a single point of failure and does not require expensive infrastructure since all operations are performed on users' machines. Moreover, flexibility of web of trust is beyond the scope of centralized systems. Highly appreciated by freedom-fighters, provides strong cryptography for individuals with almost no cost in terms of money.

\subsubsection{Drawbacks}
Web of trust requires a lot of trust between pairs of users, and consequently, might be vulnerable to various manipulations. For example, let's say there is a relation of trust between Andrew and Isabel as in Fig. \ref{fig:weboftrust}, and Alice did not verify Isabel's key but intentionally accepted a flawed one. Now the chain of trust is susceptible to attack since someone can read Andrew's message addressed to Isabel without their permission. This problem could be solved by voting if there were more chains of trust between Andrew and Isabel (bypassing Alice) or by using a separate channel so that Andrew can check Isabel's key.

What is more, users must spend their time and gain some knowledge on certification, which often causes problems to newcomers, because it demands some experience and a lot of care. Ease of use might be very problematic to non-technical people because users have to perform the same tasks as servers in centralized schemes. According to a very informative paper \cite{whittenwhy} only four out of twelve motivated and experienced email users managed to correctly send encrypted email using PGP 5.0. Although the paper concerns rather old version of software, the results are likely to stand as handling basic tasks (i.e. encryption, publishing/getting keys, key verification) did not change much over time.

Finally, people who live in remote areas and do not have many friends, might seem less credible, and thus discriminated. This can be problematic, because it can be hard for a person to be trusted right away and fully benefit from using security software unless there are other users who can introduce the person.

In conclusion, if cryptography is to be used by masses, it should be transparent and easy to use. All of the mentioned issues can be, more or less, solved by centralized schemes, such as public-key infrastructure.

\subsection{Public-Key Infrastructure}
Public-key infrastructure is the most popular solution for proving authenticity of public keys. Similarly to web of trust, it applies certificates to confirm relation between user and his public key. On the other hand, PKI's model is centralized and hierarchical, composed of special nodes called Registration Authority (RA) \nomenclature{RA}{Registration Authority} and Certificate Authority (CA)\nomenclature{CA}{Certificate Authority}, who make up the infrastructure. The authorities are trusted third parties, which are not run by ordinary users.

The role of RA and CA is as follows. Registration Authority collects requests from users to issue digital certificate for their public keys. However, before CA can sign the key, Registration Authority must verify credentials of the client. Upon successful verification, Certificate Authority generates certificate, which contains user's key, identity and CA's signature. 

The fundamental question is: who granted CA the right to authorize users' keys? Usually it is another Certificate Authority, who is even more trusted. PKI forms hierarchical structure in which CA's keys are further signed by other CA's. However, root Certificate Authorities sign their keys themselves and for this reason those certificates are usually deployed with software.

\subsubsection{Advantages}
Public-key infrastructure is well-known for its scalability and high level of security if used correctly. PKI is able to guarantee privacy, authentication, integrity and non-repudiation services to its users. It was deployed on large scale in many organizations and is the most common cryptography-related feature on the Internet. Therefore, many issues were precisely identified and addressed, such as:

\begin{itemize}
  \item securing Certificate Authorities, which are PKI's single points of failure,
  \item careful identity checking of the certificate holder to create valid certificate,
  \item moving from Certificate Revocation Lists to on-line status query mechanisms to avoid computational and bandwidth overhead,
  \item naming semantics in certificates by moving from global to more local structures to avoid name ambiguity,
\end{itemize}

Moreover, many people find public-key infrastructure relatively easy to use because they do not have to perform tedious tasks, such as finding a link of trust or manual key certification. In contrary to web of trust, a person who receives a newly created certificate can forthwith fully benefit from using strong cryptography.

\subsubsection{Drawbacks}
Although, PKI works in practice, it has significant drawbacks. Evidently, PKI does not solve many problems as it was expected to do. Firstly, the infrastructure is heavy-weight and rather expensive, because requires trusted authorities to obey strict security policies. These rules concern both digital and physical security measures to protect Certificate Authority from compromise.

Moreover, certificates must be verified by users (whether they match correct identity), but non-technical users usually fail to do it right. Even though the checking process is not that complicated (e.g comparing web address with holder specified in certificate) many people do not perform it. One solution is to make software do the checks but this approach has its limitations in real-world settings. Perhaps another solution, yet more radical, would be to completely abandon certificates as it happens in identity-based encryption.

Another drawback is certificate management and revocation of old/compromised keys. The main problem of key validation is how to quickly check whether particular key is up-to-date. Much of computational overhead comes from validation of full certification paths. So called Certificate Revocation Lists are original mechanism utilized by Public-Key Infrastructure. In practice, they might grow rapidly and become awkward to manage, which is usually the case in big deployments of PKI. Alternatively, there exists an on-line approach which moves much of the validation overhead from clients to dedicated servers that constitute to additional infrastructure.

Finally, if certificate is to match particular person, it must contain personal details such as name, surname, but in global world this still might not be precise enough, because names can repeat. The problem can be resolved by leaning towards local namespaces and providing more identity details. Unfortunately, the latter can lead to privacy compromise and might not be accepted by individual users. A different approach might be to use simple identities (e.g. email address) in place of proper names. More issues related with PKI are described in \cite{pkimyths}. 

A striking example of improper PKI deployment are SSL\nomenclature{SSL}{Secure Sockets Layer} certificates, which are synonym of secure e-commerce. First of all, in contrary to popular belief concerning web security, they only guarantee that public key matches specified URL address. Companies often outsource financial matters to other companies, and when user connects with the original web-site, she is later redirected to SSL protected web-page whose holder field in certificate does not match the original site. Users and web browsers accept this fact, even if it directly contradicts the idea of SSL certificates. The next point in SSL discussion is the method of certificate deployment. As mentioned earlier root certificates come with software, because they cannot be reliably fetched over unsecured protocol. Therefore, web browsers usually come with a set of certificates. The problem is, by default one downloads the browser over unsecured HTTP protocol, which can result in man-in-the-middle\nomenclature{MITM}{Man-In-The-Middle} attack both on the browser and enclosed certificates. Producers of web browsers could easily resolve the problem but they tend to ignore it. To sum up, issues with SSL do not lie in certificates but rather in implementation of the PKI idea.

To conclude, public-key infrastructure automates many tasks which must be handled manually by web of trust users. The best solution would be to make security-related features totally transparent, so that people could use them without any particular knowledge or extra checks. Schemes which offer this kind of functionality can be built upon identity-based cryptography (ID-PKC)\nomenclature{ID-PKC}{Identity-Based Public-Key Cryptography}.

\subsection{Identity-Based Encryption}
Aims of public key infrastructure and identity-based encryption (IBE)\nomenclature{IBE}{Identity-Based Encryption} are quite similar but the way they approach certain problems are slightly different. First of all, users of IBE can set public key to be an arbitrary, yet unique, string. Therefore, the key can be something easily memorable like an email address or a phone number. Secondly, there are no certificates binding user with his public key; Bob's unique ID\nomenclature{ID}{Identity} string guarantees that no user besides him should be able to decrypt the content.

The above cryptosystem was firstly proposed by Shamir \cite{19483} in 1984 primarily to simplify certificate management in email systems. Besides, IBE allows to implement transparent data encryption in various communication systems, such as email or cellular telephony. Although transparent encryption is not important from theoretical point of view, it is a significant factor in real-world implementations. Usually non-technical users have no knowledge on computer security and for this reason misuse security-related software. Phishing, perhaps, is the best example of how to deceive average user and make him reveal sensitive data even if the connection seems protected properly.

IBE relies on trusted third party, often called Private Key Generator (PKG)\nomenclature{PKG}{Private Key Generator}, which is responsible for generating secret keys corresponding to users' public keys. PKG has its own key pair called master public key and master private key (aka master key). The latter is involved in the process of creating  user's private key from given identity.

The first practical implementation of IBE appeared in 2001 and was presented by Boneh and Franklin (BF)\nomenclature{BF}{Identity-based encryption Boneh and Franklin scheme} \cite{704155}. The BF scheme makes use of pairings (namely the Weil pairing) over elliptic curves and finite fields. Security of the scheme is based on elliptic curve analogue of the computational Diffie-Hellman assumption, so the underlying mathematical problem is hardness of finding discrete logarithms in finite cyclic groups.

\begin{figure}[h!]
  \centering
  \includegraphics{img/ibe}
  \caption{Scheme of Identity-Based Encryption}
  \label{fig:ibe}
\end{figure}

\subsubsection{Algorithms}
Implementation of IBE cryptosystem relies on four randomized algorithms:
\begin{description}
  \item[\texttt{Setup}]
    Usually run by the PKG to create global public parameters and master key pair. The algorithm takes security parameter $k$ which determines strength of cryptosystem. Public parameters $\mathcal{P}$ include a description of finite message space $\mathcal{M}$ and ciphertext space $\mathcal{C}$. Public parameters $\mathcal{P}$ are uploaded onto Public Parameters Server (PPS)\nomenclature{PPS}{Public Parameters Server} and published. Master key $s$ is kept secret and is known only to PKG.
  \item[\texttt{Extract}]
    The algorithm is run by the PKG when one requests for her private key. Input parameters are public parameters $\mathcal{P}$, master secret $s$ and identity string ID $\in \left\{0,1\right\}^*$. The function returns user's private key $d$ corresponding to given identity.
  \item[\texttt{Encrypt}]
    The procedure is invoked by the sender to encrypt message using specified identity. As input parameters takes public parameters $\mathcal{P}$, identity ID and message $M \in \mathcal{M}$. As output returns ciphertext $C \in \mathcal{C}$.
  \item[\texttt{Decrypt}]
    Executed by the receiver to decrypt message using corresponding private key. Takes private key $d$, public parameters $\mathcal{P}$, identity ID, ciphertext $C$ as input and outputs message $M$.
\end{description}

\subsubsection{Advantages}
IBE has several features not present in traditional PKI. First of all, users' public keys have very attractive form, which is much more user-friendly than numerical keys. Thanks to identities, there is no need for certificates and heavy-weight authorities. In principle, one could provide encryption services to every email user without any vetting. Moreover, it is possible to encipher message even if the recipient has not generated key pair yet. This approach reduces much of bandwidth and organizational overhead. In comparison to PKI, revocation of keys is much easier because when recipient's private key becomes obsolete, the sender does not need to get a new certificate from Bob. Even if the private key changes, corresponding identity stays the same, so the sender can be completely unaware of revocation of receiver's key.

\subsubsection{Drawbacks}
On the other hand, when Bob wants to decrypt the message, he firstly contacts PKG, authenticates himself and then receives the corresponding private key. Trust placed in PKG is very high since it works as a key escrow and is capable of decrypting all the traffic. This property eliminates original IBE scheme from wide applications in big hostile networks like the Internet. However, it is still usable in closed commercial environments. Moreover, several modifications of IBE have appeared so far and fixed the above weakness. The most notable are certificate-based encryption (CBE)\nomenclature{CBE}{Certificate-Based Encryption} and certificateless cryptography (CL-PKE) schemes.

\subsection{Certificate-Based Encryption}
The primary goal of certificate-based encryption was to solve certificate revocation problem existing in traditional PKI. Certificate-based encryption was described by Gentry in his paper \cite{Gentry03}. The scheme is based on IBE, but eliminates some of its deficiencies, such as presence of key escrow or compulsory confidential channel between user and Private Key Generator. In fact, CBE combines the best features of PKI and identity-based cryptography.

The role of certificates in CBE is twofold, on one hand they authenticate public keys, but on the other hand they act as decryption keys. The scheme provides both implicit and explicit certifications, the former comes from certificates and the latter from identities. Message decryption can be performed only if the recipient has got private key and valid certificate from her Certificate Authority, because messages are doubly encrypted.

Users of certificate-based encryption generate own private/public keys and request certificates from CA's. Certificate Authorities compute them from users' identities, as it happens in IBE, and deliver up-to-date certificates to clients. Since certificates can be publicly known, they do not have to be transfered over protected connections. Additionally, double encryption guarantees that neither client nor CA alone is capable of decrypting the ciphertext. Similarly to IBE, users' public keys can be arbitrary strings unique within CA.

\subsubsection{Algorithms}
The four randomized algorithms making up CBE are as follows:
\begin{description}
  \item[\texttt{Setup}]
    Run by CA to create global public parameters and master key pair. The algorithm takes security parameter $k$ which determines strength of cryptosystem. The public parameters $\mathcal{P}$ include a description of finite message space $\mathcal{M}$ and ciphertext space $\mathcal{C}$. Master key $s$ is kept secret and is known only to CA.
  \item[\texttt{Certify}]
    The algorithm is run by CA when client requests certificate for his identity. Input arguments are public parameters $\mathcal{P}$, master secret $s$, period $i$ in which the certificate is valid and identity string ID that contains user's public key and  any necessary additional identifying information. The function returns certificate $Cert_B$ valid for given period.
  \item[\texttt{Encrypt}]
    The procedure is invoked by the sender to encrypt message using specified identity. As input parameters takes public parameters $\mathcal{P}$, identity ID, validity period $i$,  and message $M \in \mathcal{M}$. As output returns ciphertext $C \in \mathcal{C}$.
  \item[\texttt{Decrypt}]
    Executed by the receiver to decrypt message using corresponding private key. Takes private key $d$, public parameters $\mathcal{P}$, identity ID, certificate $Cert_B$ and ciphertext $C$ as input and outputs message $M$.
\end{description}
\subsubsection{Advantages}
Certificate-based encryption is a serious alternative to traditional PKI model. It can supply lightweight infrastructure for public-key cryptography. CBE eliminates the need for third-party queries on certificate status and hence can greatly reduce demand on extra servers. CBE preserves remarkable features of both traditional public-key infrastructure and identity-based encryption, i.e. no key escrow property, reasonable trust to trusted third party, no confidential connection with CA and two modes of certification. Computational cost of key validation can be greatly reduced by application of \emph{subset cores}, which are described in the original CBE paper. Thanks to this technique, a single CA server can handle all its clients.

\subsubsection{Drawbacks}
In comparison to IBE, user's public keys are longer, because they contain both identity and numerical part. Although, CBE removes third-party queries, it still requires online connection with clients, who regularly ask for new certificates. In practice, CBE servers would have to work non-stop to be able to provide certificates, which makes them vulnerable to DoS\nomenclature{DoS}{Denial of Service} attacks. Certificateless cryptography is somewhat similar to CBE in terms of goals and implementation, but does not require continuous work.

\subsection{Certificateless Cryptography}
Certificateless cryptography (CL-PKC)\nomenclature{CL-PKC}{Certificateless Public-Key Cryptography} was firstly presented by Al-Riyami and Paterson in their paper \cite{Al-riyami03certificatelesspublic}. The work was highly influenced by BF scheme and as a result is an extension of the original IBE. CL-PKC eliminates the key escrow feature found in the Private Key Generator. Instead, creation of private key is split between a user and trusted third party called Key Generation Center (KGC)\nomenclature{KGC}{Key Generation Center}. Consequently, user's public key is a pair composed of identity ID and public key $P_A$. The key is no longer easily memorable as in original IBE but the trust level placed on third party is much lower. It looks like functionality of CL-PKC is somewhere between traditional certified PKI and identity-based cryptography. Flexibility is one of the most significant attributes of certificateless cryptography; in fact, it can be transformed into traditional PKI or IBE. Similarly to IBE, mathematical foundations of CL-PKE come from elliptic curves and hardness of finding discrete logarithms in finite groups.

Main features of CL-PKC include the lack of key escrow property, no certificates to guarantee authenticity of public keys, the use of identities and existence of trusted third party which participates in key generation. To encrypt a message one needs public parameters, recipient's identity and public key. The use of identity in encryption prevents any other party from decrypting the content even if one tries to forge the second part of public key. Furthermore, the second part of public key (i.e. a point on elliptic curve) prevents KGC from deciphering the message.

Public key distribution works as in PKI, user publishes his public key in a public directory or attaches to outgoing emails. As long as KGC does not try to forge the key, all the encrypted data are safe. This means that KGC must be as trusted as CA in traditional public key infrastructure. However, any forbidden activity of KGC can be detected by users. In practice no CA dares to publish fake keys since it puts the company out of profitable business, so it is reasonable to assume that KGC behaves honestly. Hence, it is possible to build secure transparent email encryption which allows non-technical users to communicate confidentially.

In contrast to PKI, certificateless scheme does not require expensive infrastructure composed of different kind of authorities. Similarly to IBE, only Key Generation Center and Public Parameters Server are needed. The optimal choice is to place them per name space such as DNS zone. These two servers shall cope with all the incoming traffic.

\subsubsection{Algorithms}
Certificateless public key encryption with chosen ciphertext security (in \cite{Al-riyami03certificatelesspublic} referred to as Full CL-PKE) is built upon seven randomized algorithms:
\begin{description}
  \item[\texttt{Setup}]
    Usually run by the KGC to create public parameters and master key pair. The algorithm's input is security parameter $k$ which determines strength of cryptosystem. The output consists of public parameters $\mathcal{P} = \langle \mathbb{G}_1, \mathbb{G}_2, \mathbb{G}_T, e, n, P, P_{pub}, H_1,$ $H_2, H_3, H_4\rangle$, description of finite message space $\mathcal{M} = \{0,1\}^n$ and ciphertext space $\mathcal{C} = \mathbb{G}_2 \times \{0,1\}^{2n}$ and master key $s$. Public parameters are uploaded onto PPS and published afterwards. Master key $s \in Z_q^{\ast}$ is kept secret and is known only to KGC.
  \item[\texttt{Partial-Private-Key-Extract}]
    The algorithm is run by the KGC when one requests for her private key. Input arguments are public parameters $\mathcal{P}$, master secret $s$ and an identity string ID$_A$ $\in \left\{0,1\right\}^*$. The output is partial private key $D_A$ corresponding to given identity.
  \item[\texttt{Set-Secret-Value}]
    Run by user to generate a secret value $x_A$. In general case public parameters $\mathcal{P}$ and appropriate identifier ID are inputs of the algorithm.  \item[\texttt{Set-Private-Key}]
    The algorithm computes user's private key $S_A \in \mathbb{G}_2^{\ast}$ from public parameters $\mathcal{P}$, partial private key $D_A$ and the secret value $x_A \in Z_q^{\ast}$.
  \item[\texttt{Set-Public-Key}]
    The algorithm computes user's public key $P_A$ from public parameters $\mathcal{P}$ and secret value $x_A$.
   \item[\texttt{Encrypt}]
    Invoked by the sender to encrypt message using specified identity. Takes public parameters $\mathcal{P}$, identity ID$_A$, public key $P_A = \langle X_A, Y_A \rangle$, message $M \in \mathcal{M}$ and returns ciphertext $C \in \mathcal{C}$. Providing that the public key is corrupted, the algorithm returns $\perp$.   
  \item[\texttt{Decrypt}]
    Executed by the receiver to decrypt message using the corresponding private key. As input takes private key $S_A$, public parameters $\mathcal{P}$, identity ID$_A$, ciphertext $C \in \mathcal{C}$ and outputs message $M$. Providing that the message is corrupted, the algorithm returns $\perp$.
\end{description}
   
\subsubsection{Advantages}
Certificatelesss cryptography can supply one of the most flexible infrastructures for public-key cryptography. It combines the best aspects of both traditional public-key infrastructure and identity-based encryption, such as lack of certificates, no key escrow property, reasonable trust to trusted third party and lightweight infrastructure. Applications of CL-PKE can be the same as for PKI and IBE, that is companies' networks, the Internet and consumer electronics devices. As with IBE, certificateless cryptography can be used as underlying mechanism for transparent email/sms encryption.

\subsubsection{Drawbacks}
Even thought public keys are not as simple as in identity-based encryption, it is still possible to fetch the numerical part of the key from given identity. To provide this kind of service, CL-PKE would have to incorporate a kind of public-key directory, present in web of trust. Furthermore, performed computations are rather complicated and expensive, so there is desire for faster algorithms before real-world systems can be implemented. Finally, original version of CL-PKE reduces certificates only for users, but preserves them on connections with PPS and KGC servers. To completely eliminate certificates, servers of the scheme would have to belong to hierarchical CL-PKE. Then, the public parameters and key of root server would be deployed together with software (as root CA's certificates in PKI).

\section{Elliptic Curve Cryptography}
Elliptic curves have numerous applications within public-key cryptography, and for this reason their own field is known as elliptic curve cryptography. They supply basic mechanisms for identity-based and certificateless public encryption schemes. Although elliptic curves are objects described by general equation:
\begin{equation}
  Y^2 = X^3 + aX + b, 
  \label{eq:ecc}
\end{equation}
they are typically defined over finite fields when used within cryptography. Elliptic curves are attractive structures, because they allow to construct cryptosystems whose security relies upon hardness of finding discrete logarithms (ECDLP)\nomenclature{ECDLP}{Elliptic Curve Discrete Logarithm Problem}. Having points $P$ and $Q$, where $Q = kP$, there is no efficient algorithm for finding $k$. The most significant advantages of ECC include shorter keys and fewer computations than in other popular schemes (RSA, DH\nomenclature{DH}{Diffie-Hellman}), while preserving the same level of security. An overall comparison may be found in Tab. \ref{tab:ksecparam}, where $k$ is RSA public-key bit-length and $n_q$ is length of corresponding ECC key.

Elliptic curve over finite field is a set of points $(x,y) \in \mathbb{F}_q \times \mathbb{F}_q$ satisfying (\ref{eq:ecc}) plus a point at infinity $\mathcal{O}$ being identity element. These points form a finite Abelian group $(E(\mathbb{F}_q),+)$ with negation of point $P = (x,y)$ being $-P = (x,-y)$ and operation of addition dependent on $q$, but holding the following properties:
\begin{itemize}
  \item if $Q = \mathcal{O}$ then $P + Q = P$
  \item if $Q = -P$ then $P + Q = \mathcal{O}$
  \item if $Q \neq P$ then $P + Q = R \in E(\mathbb{F}_q)$.
\end{itemize}
The above facts allow us to construct operation of point multiplication, that is, for given $k$ it is straightforward to compute point $Q = kP$, since:
\begin{equation}
  kP = \underbrace{P + P + \cdots + P}_\text{k}.
  \label{eq:eccadd}
\end{equation}
However, what is really utilized by elliptic curve cryptography is the cyclic subgroup of $E(\mathbb{F}_q)$, because for any point $P$ the set $\{\mathcal{O}, P, 2P, 3P, \ldots\}$ is a cyclic group. Of course the subgroup is much smaller (as comparison of $n_p$ and $n_q$ in Tab. \ref{tab:ksecparam} indicates). Elliptic curve discrete logarithm problem is built on top of the cyclic subgroup.

The area of elliptic curves is unbelievably wide and certainly out of scope of this dissertation. The above introduction shall be enough for understanding mathematical foundations of certificateless cryptography. Basic algorithms required by the project are described in book \cite{345194}. Furthermore, elliptic curves alone are not sufficient to construct identity-based schemes. These schemes incorporate pairings, which are mappings between groups.

\section{Pairings}
Pairing-based cryptography is a field in which cryptosystems are constructed upon pairings. Most of identity-based schemes, among which are IBE, CBE and CL-PKE, belong to this area. Generally speaking, pairing is a map between elements of two groups and a third group. In practice, it allows to solve certain problem in one group, even if the problem is said to be hard in another group. More detailed information about pairings and their applications in the context of elliptic curve cryptography can be found in \cite{lynnphd}.

Although pairing is a more general concept, its definition within cryptography is as follows. Let $\mathbb{G}_1,\mathbb{G}_T$ be cyclic groups of prime order $q$ and $\mathbb{G}_2$ group where each element has order dividing $q$. Moreover, groups $\mathbb{G}_1, \mathbb{G}_2$ are additive and $\mathbb{G}_T$ is multiplicative. Then, we say that map $e : \mathbb{G}_1 \times \mathbb{G}_2 \to \mathbb{G}_T$ is an admissible pairing if satisfies the following properties:
\begin{enumerate}
  \item Bilinear: $e(aP,bQ) = e(P,Q)^{ab}$ for all $P\in \mathbb{G}_1, Q\in \mathbb{G}_2$ and $a,b \in \mathbb{Z}$.
  \item Non-degenerate: $e(P,Q) = 1_{\mathbb{G}_T}$ for all $Q\in \mathbb{G}_2$ if and only if $P = 1_{\mathbb{G}_1}$, and similarly $e(P,Q) = 1_{\mathbb{G}_T}$ for all $P\in \mathbb{G}_1$ if and only if $Q = 1_{\mathbb{G}_2}$
  \item Computable: There is an efficient algorithm to compute $e(P,Q)$ for any $P\in \mathbb{G}_1, Q\in \mathbb{G}_2$.
\end{enumerate}
The above definition is sometimes called the asymmetric pairing. When groups $\mathbb{G}_1$ and $\mathbb{G}_2$ are the same group, then we say that pairing is symmetric. Moreover, it may be hard to find discrete logarithms in the three groups, but the Decision Diffie-Hellman problem (DDH)\nomenclature{DDH}{Decision Diffie-Hellman problem} might be easy in $\mathbb{G}_1$ and $\mathbb{G}_2$. The DDH problem is important from theoretical point of view when it comes to security of pairing-based cryptosystems. The concrete implementations of pairings usually involve modified Weil or Tate pairing. More about the underlying problems and their precise mathematical discussions may be found in \cite{704155, Al-riyami03certificatelesspublic, Gentry03, lynnphd}.

\section{Certificateless Cryptography Algorithms}
After basic introduction to elliptic curve cryptography and pairings one is ready to understand precise definitions of CL-PKE algorithms:
\begin{description}
  \item[\texttt{Setup}]
    The algorithm comprises the following steps:
    \begin{enumerate}
      \item Generate tuple $\langle \mathbb{G}_1, \mathbb{G}_2, \mathbb{G}_T, e \rangle$ where $\mathbb{G}_1$ and $\mathbb{G}_T$ are groups of some prime order $q$, order of each element belonging to $\mathbb{G}_2$ group divides $q$ and $e \colon \mathbb{G}_1 \times \mathbb{G}_2 \to \mathbb{G}_T$ is pairing. Preferably $q$ is a Solinas prime, i.e. one of the form $q = 2^{e_2} \pm 2^{e_1} \pm 1$, where $e_1$ and $e_2$ are exponents.
      \item Choose random generator $P$ of $\mathbb{G}_1$ group.
      \item Choose random master key $s$ from $Z_q^{\ast}$ and compute public key $P_{pub} = sP$
      \item Choose cryptographic hash functions $H_1 \colon \{0,1\}^{\ast} \to \mathbb{G}_2^{\ast}$, $H_2 \colon \mathbb{G}_T \to \{0,1\}^n$, $H_3 \colon \{0,1\}^n \times \{0,1\}^n \to Z_q^{\ast}$ and $H_4 \colon \{0,1\}^n \to \{0,1\}^n$, where $n$ is length of plaintexts.
    \end{enumerate}
  \item[\texttt{Partial-Private-Key-Extract}]
    The following steps are performed to compute the partial private key:
    \begin{enumerate}
      \item Map identity to point on elliptic curve by computing $Q_A = H_1($ID$_A) \in \mathbb{G}_2^{\ast}$.
      \item Compute the partial private key $D_A = sQ_A \in \mathbb{G}_2^{\ast}$.
    \end{enumerate}
  \item[\texttt{Set-Secret-Value}]
    The algorithm chooses random $x_A \in Z_q^{\ast}$.
  \item[\texttt{Set-Private-Key}]
     Outputs user's private key by computing $S_A = x_A D_A = x_A sQ_A$.
  \item[\texttt{Set-Public-Key}]
     The algorithm returns $P_A = \langle X_A, Y_A \rangle = \langle x_A P, x_A P_{pub} \rangle = \langle x_A P, x_A sP \rangle$.
  \item[\texttt{Encrypt}]
    Message encryption runs as follows:
    \begin{enumerate}
      \item If the conditions $X_A, Y_A \in \mathbb{G}_1^{\ast}$ and $e(X_A,P_{pub}) = e(Y_A,P)$ are satisfied, move to the next step. Otherwise, return $\perp$ and stop.
      \item Compute $Q_A = H_1($ID$_A) \in \mathbb{G}_2^{\ast}$.
      \item Choose random $\sigma \in \{0,1\}^n$.
      \item Compute $r = H_3(\sigma,M)$.
      \item Compute the ciphertext $C = \langle rP, \sigma \oplus H_2(e(Q_A,Y_A)^r),M \oplus H_4(\sigma)\rangle$. Note that the most complex operation is pairing $e(Q_A,Y_A)$, but its value is constant for given identity and public key, therefore it does not have be recomputed every time.
    \end{enumerate}
  \item[\texttt{Decrypt}]
    Ciphertext $C = \langle U, V, W \rangle \in \mathcal{C}$ decryption runs as follows:
    \begin{enumerate}
      \item Compute $\sigma = V \oplus H_2(e(S_A, U))$.
      \item Compute $M = W \oplus H_4(\sigma)$.
      \item Compute $r = H_3(\sigma,M)$ and continue if $U = rP$. Otherwise, return $\perp$ and stop.
      \item Return $M$.
    \end{enumerate}
\end{description}

