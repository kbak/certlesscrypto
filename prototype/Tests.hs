module Tests where

import FiniteField
import EllipticCurves
import Prelude hiding ( (>>) )
import Numeric
import Test.QuickCheck

prop_add :: Integer -> Integer -> Integer -> Integer -> Property
prop_add x1 y1 x2 y2 =
  y1^2 == x1^3 + 1 && y2^2 == x2^3 + 1 ==>
  (p1 `pointAdd1` p2) == (p2 `pointAdd1` p1)
  where
  p1 = newPoint x1 y1
  p2 = newPoint x2 y2

prop_double :: Integer -> Integer -> Bool
prop_double x y = pointDouble1 p == pointAdd1 p p
  where
  p = newPoint x y

prop_double' :: Integer -> Integer -> Property
prop_double' x y =
  y^2 == x^3 + 1 ==> (pointDouble1.pointDouble1) p ==
  (p `pointAdd1` p `pointAdd1` p `pointAdd1` p)
  where
  p = newPoint x y
  
prop_swd :: Integer -> Integer -> Property
prop_swd k r =
  k > 0 && r > 0 ==> k == sum (map (\(b, e) -> b * 2^e) s)
  where
  (d, s) = signedWindowDecomposition k r
  
testPointMultiply = testPm' pointMultiply
testPtM = testPm' pm

xa_test :: IntZp
xa_test = N 0x489a03c58dcf7fcfc97e99ffef0bb4634

ya_test :: IntZp
ya_test = N 0x510c6972d795ec0c2b081b81de767f808

xb_test :: IntZp
xb_test = N 0x40e98b9382e0b1fa6747dcb1655f54f75

yb_test :: IntZp
yb_test = N 0xb497a6a02e7611511d0db2ff133b32a3f

p_test :: Point
p_test = Point xa_test ya_test

testPm' :: (Point -> Integer -> Point) -> String
testPm' f = showHex x " " ++ showHex y ""
  where
  (Point (N x) (N y)) = f p_test 0xb8bbbc0089098f2769b32373ade8f0daf

testWd = signedWindowDecomposition 0xb8bbbc0089098f2769b32373ade8f0daf 5

-- reference implementation
pm :: Point -> Integer -> Point
pm p k = pm' O p k

pm' :: Point -> Point -> Integer -> Point
pm' q _ 0 = q
pm' q p k
  | odd k     = pm' (pointAdd1 q p) p' k'
  | otherwise = pm' q p' k'
  where
  p' = (pointDouble1 p)
  k' = k >> 1

testPm :: Integer -> Integer -> Integer -> Property
testPm x y k =
  k > 0 ==> pm p k == pointMultiply p k
  where
  p = newPoint x y

testPairing = showHex ea " " ++ showHex eb " "
  where
  (N ea, N eb) = pairing1 (Point xa_test ya_test) (Point xb_test yb_test)