module FiniteField where

import Prelude hiding ( (>>) )

f_p :: Integer
f_p = 0xbffffffffffffffffffffffffffcffff3

modFp :: Integer -> Integer
modFp n = n `mod` f_p

gcdE :: Integer -> Integer -> (Integer, Integer, Integer)
gcdE a 0 = (a, 1, 0)
gcdE a b = (d, t, s - q * t)
  where
  r         = a `mod` b
  q         = a `div` b
  (d, s, t) = gcdE b r

inv :: Integer -> Integer
inv a = modFp t
  where
  (_, _, t) = gcdE f_p a

newtype IntZp = N { zp :: Integer } deriving Show

newIntZp :: Integer -> IntZp
newIntZp n = N (modFp n)

instance Eq IntZp where
  N a == N b   = (modFp a) == (modFp b)
  n1  /= n2    = not (n1 == n2)

instance Num IntZp where
  N a + N b    = N (modFp (a + b))
  N a - N b    = N (modFp (a - b))
  N a * N b    = N (modFp (a * b))
  negate (N a) = N (modFp (-a))
  abs    (N a) = N (modFp a)
  signum (N a) = if a' == 0 then 0 else 1
    where
    a' = modFp a
  fromInteger a = N (modFp a)

instance Fractional IntZp where
  N a / N b = N (modFp (a * bInv))
    where
    bInv = (inv.modFp) b
  fromRational a = N (modFp (truncate a))

pow :: IntZp -> Integer -> IntZp
pow a b = pow' a b (N 1)

pow' :: IntZp -> Integer -> IntZp -> IntZp
pow' _ 0 res = res
pow' b e res
  | odd e     = pow' b' e' (res * b)
  | otherwise = pow' b' e' res
  where
  b' = b * b
  e' = e >> 1

(<<) :: Integer -> Integer -> Integer
(<<) n k = shift n k (* 2)

(>>) :: Integer -> Integer -> Integer
(>>) n k = shift n k (`div` 2)

shift :: Integer -> Integer -> (Integer -> Integer) -> Integer
shift n 0 _  = n
shift n k op = shift (op n) (k - 1) op

toBin :: Integer -> [Integer]
toBin 0 = []
toBin n = toBin (n `div` 2) ++ [n `mod` 2]

lg x = (log (fromInteger x)) / log 2