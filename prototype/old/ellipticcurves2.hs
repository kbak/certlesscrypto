data Point = O | Point
               { x :: Integer
               , y :: Integer
               } deriving Show

pointDouble1 :: Point -> Point
pointDouble1 O       = O
pointDouble1 (Point _ 0) = O
pointDouble1 (Point x y) = Point x' y'
  where
  lambda = (3 * x^2) `div` (2 * y)
  x'     = lambda^2 - 2 * x
  y'     = (x - x') * lambda - y

pointAdd1 :: Point -> Point -> Point
pointAdd1 O b = b
pointAdd1 a O = a
pointAdd1 a@(Point x_A y_A) (Point x_B y_B)
  | x_A == x_B = if y_A == -y_B then O else pointDouble1 a
  | otherwise  = Point x' y'
  where
  lambda = (y_B - y_A) `div` (x_B - x_A)
  x'     = lambda^2 - x_A - x_B
  y'     = (x_A - x') * lambda - y_A

type DSequence = (Integer, [(Integer, Integer)])

signedWindowDecomposition :: Integer -> Integer -> DSequence
signedWindowDecomposition k r
  | k > 0 && r > 0 = decompose k 0 0
  | otherwise      = error "negative k or r"
  where
  l = getBitLen k

  decompose :: Integer -> Integer -> Integer -> DSequence
  decompose k d j
    | j <= l    = if getBit j k == 0 then decompose k d (j + 1)
                                        else (d', (b_d, j) : s)
    | otherwise = (d, [])
    where
    t          = min l (j + r - 1)
    h_d        = extract j t k
    (b_d, k')  = if h_d > 2^(r - 1) then (h_d - 2^r, increment j k)
                                    else (h_d, k)
    (d', s)    = decompose k' (d + 1) (t + 1)

  -- Increment the number (k_l, k_(l-1),...,k_(t+1)) !!!
  increment :: Integer -> Integer -> Integer
  increment j k = add low high (getBitLen low)
    where
    low  = extract 0 j k
    high = 1 + extract (j + 1) l k

extract :: Integer -> Integer -> Integer -> Integer
extract 0 0 k = k `mod` 2
extract 0 h k = k `mod` 2 + 2 * extract 0 (h - 1) (k `div` 2)
extract l h k = extract (l - 1) (h - 1) (k `div` 2)


(<<) :: Integer -> Integer -> Integer
(<<) n k = shift n k (* 2)

(>>) :: Integer -> Integer -> Integer
(>>) n k = shift n k (`div` 2)

shift :: Integer -> Integer -> (Integer -> Integer) -> Integer
shift n 0 _  = n
shift n k op = shift (op n) (k - 1) op

getBit :: Integer -> Integer -> Integer
getBit i n = extract i i n

getBitLen :: Integer -> Integer
getBitLen n = ceiling (log (fromInteger n) / log 2.0)

add :: Integer -> Integer -> Integer -> Integer
add l h 0   = h + l
add l h len = add l (2 * h) (len - 1)
             {-
pointMultiply :: Point -> Integer -> Point
pointMultiply p l =
  where
  (d, s) = signedWindowDecomposition l 5
  a1     = a
  a2     = pointDouble1 a

  precomputation -}