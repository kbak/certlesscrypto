/*
 * Title:
 *     CL-PKE test
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Tests for CL-PKE scheme
 */

#include "test/common/clpketest.h"

void testClPke()
{
    std::cout << "Generating system paramters... " << std::endl;
    SystemParam sp(1024);
    std::cout << "done" << std::endl;
    std::string str = "Moja bardzo dluga wiadomosc";

    element_t x;

    Key dId = ClAlgorithms::partialPrivateKeyExtract(sp, "alice@gmail.com");
    ClAlgorithms::setSecretValue(x, sp.pp);
    Key qId = ClAlgorithms::setPrivateKey(dId, x);
    PubKey pk = ClAlgorithms::setPublicKey(x, sp.pp);
    Ciphertext c = ClAlgorithms::encrypt(
            Utils::strToOctets(str), sp.pp, pk, "alice@gmail.com");
    Octets m = ClAlgorithms::decrypt(c, qId, sp.pp);

    std::cout << ((Utils::octetsToStr(m) == str) ? "OK" : "Error") << std::endl;
    element_clear(x);
}

void testClPkeIbe()
{
    // Tests whether CL-PKE implies IBE. Note: rho in encrypt must be the same!

    std::cout << "Generating system paramters... " << std::endl;
    SystemParam sp(1024);
    std::cout << "done" << std::endl;
    std::string str = "Moja bardzo dluga wiadomosc";

    element_t x;

    // CL-PKE with x_s = 1
    {
        Key dId = ClAlgorithms::partialPrivateKeyExtract(sp, "alice@gmail.com");
        // secret value x_s = 1
        element_init_Zr(x, sp.pp.pairing);
        element_set1(x);
        Key qId = ClAlgorithms::setPrivateKey(dId, x);
        PubKey pk = ClAlgorithms::setPublicKey(x, sp.pp);
        Ciphertext c = ClAlgorithms::encrypt(
            Utils::strToOctets(str), sp.pp, pk, "alice@gmail.com");
        for (size_t i = 0; i < c.w.size(); ++i)
        {
            std::cout << std::hex << static_cast<unsigned int>(c.w[i]) << ' ';
        }
        Octets m = ClAlgorithms::decrypt(c, qId, sp.pp);

        std::cout << std::endl << ((Utils::octetsToStr(m) == str) ? "OK" : "Error") << std::endl;
        element_clear(x);
    }

    // IBE
    {
        // private key
        Key dId = ClAlgorithms::partialPrivateKeyExtract(sp, "alice@gmail.com");
        PubKey pk(sp.pp.pairing);
        element_set(pk.xPub, sp.pp.p);
        element_set(pk.yPub, sp.pp.pPub);
        Ciphertext c = ClAlgorithms::encrypt(
            Utils::strToOctets(str), sp.pp, pk, "alice@gmail.com");
        for (size_t i = 0; i < c.w.size(); ++i)
        {
            std::cout << std::hex << static_cast<unsigned int>(c.w[i]) << ' ';
        }
        Octets m = ClAlgorithms::decrypt(c, dId, sp.pp);

        std::cout << std::endl << ((Utils::octetsToStr(m) == str) ? "OK" : "Error") << std::endl;
        element_clear(x);
    }
}
