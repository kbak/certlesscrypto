/*
 * Title:
 *     Message test
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Tests for message encryption/decryption
 */

#include "test/common/messagetest.h"

void testMessageCrypt()
{
    Octet plaintext[] = {
        0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
        0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};
    Octet testVector[] = {
        0x8e, 0xa2, 0xb7, 0xca, 0x51, 0x67, 0x45, 0xbf,
        0xea, 0xfc, 0x49, 0x90, 0x4b, 0x49, 0x60, 0x89};
    Octet cek[] = {
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
        0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
        0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    Octets vCek(cek, cek + sizeof(cek));
    Octets vPlaintext(plaintext, plaintext + sizeof(plaintext));
    Octets vTestVector(testVector, testVector + sizeof(testVector));

    Message m(Utils::octetsToStr(vPlaintext),
        "test@mail.com", vCek, "localhost", 6001);

    m.msg = m.doCryptMsg(true);
    // without padding
    Octets ciphertext(&m.msg[0], &m.msg[16]);

    std::cout << "AES ecryption: ";
    for (size_t i = 0; i < m.msg.size(); ++i)
    {
        std::cout << std::hex << static_cast<unsigned int>(m.msg[i]) << ' ';
    }

    std::cout << std::endl << ((vTestVector == ciphertext) ? "OK" : "Error")
        << std::endl;

    Octets deciphertext(m.doCryptMsg(false));

    std::cout << "AES decryption: "
        << ((vPlaintext == deciphertext) ? "OK" : "Error") << std::endl;
}

void testMessageBase64()
{
    Message m("moja wiadomosc", "test@mail.com", Octets(), "localhost", 6001);

    m.genCek();
    m.encryptMsg();
    std::cout << "encoded: " << Utils::octetsToStr(m.msg) << std::endl;
    m.decryptMsg();
    std::cout << "decoded: " << Utils::octetsToStr(m.msg) << std::endl;
}
