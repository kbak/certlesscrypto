/*
 * Title:
 *     HashFun test
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Tests for hash functions
 */

#include "test/common/hashfuntest.h"

void testHashToRange()
{

    std::string s    = "This ASCII string without null-terminator";
	std::string ns   = "ffffffffffffffffffffefffffffffffffffffff";
	std::string ress = "79317c1610c1fc018e9c53d89d59c108cd518608";
	mpz_t v;
	mpz_t n;
	mpz_t res;

	mpz_init(n);
	mpz_init(v);
	mpz_init(res);

	mpz_set_str(n, ns.c_str(), 16);
	mpz_set_str(res, ress.c_str(), 16);

	HashFun::hashToRange(v, n, Utils::strToOctets(s), HashFun::sha1);

	std::cout << "s = " << s << std::endl << "n = ";
	mpz_out_str(NULL, 16, n);
	std::cout << std::endl << "v = ";
	mpz_out_str(NULL, 16, v);
	std::cout << std::endl << (0 == mpz_cmp(v, res) ? "OK" : "Error")
		<< std::endl;

	mpz_clear(n);
	mpz_clear(v);
	mpz_clear(res);
}
