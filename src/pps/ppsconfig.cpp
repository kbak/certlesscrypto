/*
 * Title:
 *     PPS Configuration
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Configuration of PPS
 */

#include "pps/ppsconfig.h"

PpsConfig::PpsConfig()
    : sc(servConf)
    , login("login")
    , pass("pass")
    , m_pp(NULL)
{
    sc.nThreads = 0;
    sc.certFile = "pps.pem";
    sc.certPass = "ppsKeyPass";
    sc.port     = 6001;
}

PpsConfig::~PpsConfig()
{
    delete m_pp;
}

void PpsConfig::load() throw(ClException)
{
    std::ifstream input(ppsAuthConf.c_str());

    try
    {
        sc.load();
        if (NULL == m_pp)
        {
            m_pp = new PubParam();
        }

        try
        {
            m_pp -> load(ppsPpConf);
        } catch(...) {}
    }
    catch (ClException e)
    {
        throw ClException("PpsConfig::load:\n" + e.toString());
    }

    input >> login >> pass;
    input.close();
}

void PpsConfig::save() throw(ClException)
{
    std::ofstream output(ppsAuthConf.c_str());

    if (NULL != m_pp)
    {
        m_pp -> save(ppsPpConf);
    }
    sc.save();
    output << login << std::endl << pass << std::endl;
    output.close();
}

PubParam & PpsConfig::getPubParam() const throw(ClException)
{
    if (NULL == m_pp)
    {
        throw ClException("PpsConfig::getPubParam: Empty public parameters");
    }
    else
    {
        return (*m_pp);
    }
}

void PpsConfig::setPubParam(const PubParam & pp)
{
    delete m_pp;
    m_pp = new PubParam(pp);
}

std::ostream & operator<<(std::ostream & os, const PpsConfig & c)
{
    try
    {
        os << c.getPubParam();
    }
    catch (ClException e)
    {
        os << "PpsConfig::operator<<: Empty Public Parameters" << std::endl;
        e.print();
    }

    os << c.sc
       << "PPS Login" << std::endl << Utils::ident(c.login)
       << "PPS Password" << std::endl << Utils::ident(c.pass) << std::endl;

    return os;
}
