/*
 * Title:
 *     PPS Server Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     PPS server socket
 */

#include "pps/ppsserversocket.h"

PpsServerSocket::PpsServerSocket(ISocketHandler & h)
    : SslServerSocket(h)
{
}

PpsHandler & PpsServerSocket::getPpsHandler() const
{
    return static_cast<PpsHandler &>(MasterHandler());
}

void PpsServerSocket::onPubParamReqPacket(const PubParamReqPacket & packet)
{
    try
    {
        send(PubParamPacket(getPpsHandler().conf.getPubParam()));
    }
    catch (ClException e)
    {
        send(InfoPacket(PACKET_NO_PUBPARAM));
        std::cout << "PpsServerSocket::onPubParamReqPacket:" << std::endl;
        e.print();
    }
}

void PpsServerSocket::onPubParamSetPacket(const PubParamSetPacket & packet)
{
    if (getPpsHandler().conf.login == packet.login &&
        getPpsHandler().conf.pass  == packet.pass)
    {
        getSslHandler().pp = packet.pp;
        std::cout << "New Public Parameters" << std::endl
            << getPpsHandler().conf.getPubParam();
        getPpsHandler().conf.save();
        send(InfoPacket(PACKET_SUCCESS));
    }
    else
    {
        send(InfoPacket(PACKET_AUTH_FAILED));
        std::cout << "PpsServerSocket::onPubParamSetPacket:" << std::endl
            << "Authentication failed. Cannot setup new Public Parameters";
    }
    std::cout << std::endl;
}
