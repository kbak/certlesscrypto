/*
 * Title:
 *     PPS
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Public Parameters Server
 *
 * NOTE:
 *     <note>
 */

#include <Mutex.h>
#include <ListenSocket.h>
#include <Lock.h>
#include "common/utils.h"
#include "common/clalgorithms.h"
#include "pps/ppshandler.h"
#include "pps/ppsserversocket.h"
#include "pps/ppsconfig.h"

/// Sets up PPS (Public Parameters Server)
void setup(Arguments & arguments)
{
    PpsConfig ppsConfig;
    std::stringstream ss;
    unsigned long n = 0;

    ss << arguments["setup"];
    ss >> n;

    Utils::setConfig(arguments, "port", ppsConfig.sc.port);
    Utils::setConfig(arguments, "login", ppsConfig.login);
    Utils::setConfig(arguments, "pass", ppsConfig.pass);
    Utils::setConfig(arguments, "certfile", ppsConfig.sc.certFile);
    Utils::setConfig(arguments, "certpass", ppsConfig.sc.certPass);

    ppsConfig.sc.nThreads = 0;

    std::cout << ppsConfig;
    ppsConfig.save();
}

/// Runs PPS server
void run(Arguments & arguments, PpsConfig & ppsConfig)
{
    Mutex lock;
    PpsHandler h(lock, ppsConfig);
    ListenSocket<PpsServerSocket> l(h);

    if (l.Bind(ppsConfig.sc.port))
    {
        std::cerr << "Bind() failed" << std::endl;
        exit(-1);
    }
    Utility::ResolveLocal();
    h.Add(&l);
    while (true)
    {
        h.Select();
    }
}

int main(int argc, char * argv[])
{
    Arguments arguments = Utils::parseParams(argc, argv);
    PpsConfig ppsConfig;

    if (arguments.end() != arguments.find("setup"))
    {
        setup(arguments);
    }
    else
    {
        try
        {
            ppsConfig.load();
            Utils::setConfig(arguments, "port", ppsConfig.sc.port);
            Utils::setConfig(arguments, "login", ppsConfig.login);
            Utils::setConfig(arguments, "pass", ppsConfig.pass);
            Utils::setConfig(arguments, "certfile", ppsConfig.sc.certFile);
            Utils::setConfig(arguments, "certpass", ppsConfig.sc.certPass);
            std::cout << ppsConfig;
            run(arguments, ppsConfig);
        }
        catch (ClException e)
        {
            e.print();
        }
    }
    return 0;
}

