/*
 * Title:
 *     KGC Client Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Used by KGC to connect with PPS
 *
 * NOTE:
 *     <note>
 */

#ifndef _KGCCLIENTSOCKET_H_
#define _KGCCLIENTSOCKET_H_

#include <iostream>
#include <string>
#include "common/sockets/sslclientsocket.h"

class KgcClientSocket
    : public SslClientSocket
{
protected:

    std::string m_login;
    std::string m_pass;

public:

    KgcClientSocket(ISocketHandler & h, const std::string & login,
        const std::string & pass);

    void OnConnect();

    void OnConnectFailed();

    void onInfoPacket(const InfoPacket & packet);
};

#endif // _KGCCLIENTSOCKET_H_
