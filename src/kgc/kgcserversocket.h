/*
 * Title:
 *     KGC Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     KGC sockets
 *
 * NOTE:
 *     <note>
 */

#ifndef _KGCSOCKET_H_
#define _KGCSOCKET_H_

#include "common/utils.h"
#include "common/packets/packet.h"
#include "common/packets/pubparampacket.h"
#include "common/packets/errorpacket.h"
#include "common/sockets/sslserversocket.h"
#include "kgc/kgchandler.h"

class KgcServerSocket
    : public SslServerSocket
{
public:

    KgcServerSocket(ISocketHandler & h);

    KgcHandler & getKgcHandler() const;

    void onKeyReqPacket(const KeyReqPacket & packet);
};

#include "kgc/genkeytask.h"

#endif  // _KGCSOCKET_H_
