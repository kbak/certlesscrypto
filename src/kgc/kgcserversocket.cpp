/*
 * Title:
 *     KGC Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     KGC sockets
 */

#include "kgc/kgcserversocket.h"

KgcServerSocket::KgcServerSocket(ISocketHandler & h)
    : SslServerSocket(h)
{
}

KgcHandler & KgcServerSocket::getKgcHandler() const
{
    return static_cast<KgcHandler &>(MasterHandler());
}

void KgcServerSocket::onKeyReqPacket(const KeyReqPacket & packet)
{
    if (true) // TODO authentication
    {
        getSslHandler().threadPool.add(
            new GenKeyTask(this, getKgcHandler(),
                getKgcHandler().conf.getSystemParam(), packet.login));
    }
    else
    {
        send(InfoPacket(PACKET_AUTH_FAILED));
    }
}
