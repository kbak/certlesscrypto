/*
 * Title:
 *     KGC global parameters
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Configuration and global resources of KGC
 */

#include "kgc/kgcconfig.h"

KgcConfig::KgcConfig()
    : sc(servConf)
    , cc(clientConf)
    , ppsLogin("login")
    , ppsPass("pass")
    , m_sp(NULL)
{
    sc.nThreads = 2;
    sc.certFile = "kgc.pem";
    sc.certPass = "kgcKeyPass";
    cc.addr     = "localhost";
    cc.port     = 6001;
}

KgcConfig::~KgcConfig()
{
    delete m_sp;
}

void KgcConfig::load() throw(ClException)
{
    try
    {
        sc.load();
        cc.load();
        if (NULL == m_sp)
        {
            m_sp = new SystemParam();
        }
        m_sp -> load(kgcSpConf, kgcPpConf);
    }
    catch (ClException e)
    {
        throw ClException("KgcConfig::load:\n" + e.toString());
    }
}

void KgcConfig::save() throw(ClException)
{
    if (NULL == m_sp)
    {
        throw ClException("KgcConfig::save: Empty system parameters");
    }
    else
    {
        sc.save();
        cc.save();
        m_sp -> save(kgcSpConf, kgcPpConf);
    }
}

SystemParam & KgcConfig::getSystemParam() const throw(ClException)
{
    if (NULL == m_sp)
    {
        throw ClException("KgcConfig::getSystemParam: Empty system parameters");
    }
    else
    {
        return (*m_sp);
    }
}

void KgcConfig::setSystemParam(const SystemParam & sp)
{
    delete m_sp;
    m_sp = new SystemParam(sp);
}

std::ostream & operator<<(std::ostream & os, const KgcConfig & c)
{
    try
    {
        os << c.getSystemParam();
    }
    catch (ClException e)
    {
        os << "KgcConfig::operator<<: Empty System Parameters" << std::endl;
        e.print();
    }

    os << c.sc << std::endl << c.cc
       << std::endl << "KGC configuration" << std::endl
       << "PPS login" << std::endl
       << Utils::ident(c.ppsLogin)
       << "PPS password" << std::endl
       << Utils::ident(c.ppsPass);

    return os;
}
