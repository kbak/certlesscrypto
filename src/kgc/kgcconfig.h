/*
 * Title:
 *     KGC Configuration
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Configuration of KGC
 *
 * NOTE:
 *     <note>
 */

#ifndef _KGCCONFIG_H_
#define _KGCCONFIG_H_

#include "common/serverconfig.h"
#include "common/clientconfig.h"
#include "common/systemparam.h"
#include "common/clexception.h"

namespace
{
    const std::string servConf   = ".kgcservconfig";
    const std::string clientConf = ".kgcclientconfig";
    const std::string kgcSpConf  = ".kgcspconfig";
    const std::string kgcPpConf  = ".kgcppconfig";
}

class KgcConfig
{
public:

    ServerConfig sc;
    ClientConfig cc;
    std::string  ppsLogin;
    std::string  ppsPass;

private:

    SystemParam * m_sp;

public:

    KgcConfig();

    ~KgcConfig();

    /// Loads configuration from file
    void load() throw(ClException);

    /// Saves configuration
    void save() throw(ClException);

    SystemParam & getSystemParam() const throw(ClException);

    void setSystemParam(const SystemParam & sp);

    friend std::ostream & operator<<(std::ostream & os, const KgcConfig & c);
};

#endif  // _KGCCONFIG_H_
