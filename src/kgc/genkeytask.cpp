/*
 * Title:
 *     Generate Key Task
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Generates (partial) private key from given ID
 */

#include "kgc/genkeytask.h"

GenKeyTask::GenKeyTask(KgcServerSocket * socket, SocketHandler & h,
    const SystemParam & sp, const std::string & id)
    : m_socket(socket)
    , m_h(h)
    , m_sp(sp)
    , m_id(id)
{
}

void GenKeyTask::run()
{
    Key k = ClAlgorithms::partialPrivateKeyExtract(m_sp, m_id);
    KeyPacket kp(k);
    {
        Lock lck(m_h.GetMutex());

        std::cout << std::endl << "Private key generation" << std::endl
                    << "ID" << std::endl << Utils::ident(m_id)
                    << "(Partial) " << k << std::endl;

        if (m_h.Valid(static_cast<TcpSocket *>(m_socket)))
        {
            m_socket -> send(kp);
        }
    }
}
