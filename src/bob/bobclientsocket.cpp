/*
 * Title:
 *     KGC Client Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Used by Bob to setup key-pair
 */

#include "bob/bobclientsocket.h"

BobClientSocket::BobClientSocket(ISocketHandler & h, BobConfig & bc)
    : SslClientSocket(h)
    , m_bobConfig(bc)
{
}

void BobClientSocket::OnConnect()
{
    send(KeyReqPacket(m_bobConfig.kgcLogin, m_bobConfig.kgcPass));
    std::cout << std::endl << "Key request pending... ";
}

void BobClientSocket::OnConnectFailed()
{
    std::cout << "failed\n[error] Cannot connect to KGC" <<std::endl;
}

void BobClientSocket::onKeyPacket(const KeyPacket & packet) throw(ClException)
{
    element_t x;

    ClAlgorithms::setSecretValue(x, m_bobConfig.getPubParam());

    m_bobConfig.getKey()    = ClAlgorithms::setPrivateKey(packet.k, x);
    m_bobConfig.getPubKey() = ClAlgorithms::setPublicKey(x,
        m_bobConfig.getPubParam());
    element_clear(x);

    std::cout << "done" << std::endl
        << "Partial " << packet.k
        << "Random x" << std::endl
        << Utils::ident(Utils::elemToStr(x))
        << m_bobConfig.getKey()
        << m_bobConfig.getPubKey();
    SetCloseAndDelete();
}

void BobClientSocket::onInfoPacket(const InfoPacket & packet)
{
    std::string msg;

    switch (packet.code)
    {
    case PACKET_AUTH_FAILED:
        msg = "failed\n[error] Authorization failed. \
            Check your login & password";
        break;
    case PACKET_INVALID:
        msg = "failed\n[error] Invalid Key request";
        break;
    default:
        msg = "failed\n[error] Unexpected error";
    }

    std::cout << msg << std::endl;
    SetCloseAndDelete();
}
