/*
 * Title:
 *     Bob Server Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Bob server socket
 */

#include "bob/bobserversocket.h"

BobServerSocket::BobServerSocket(ISocketHandler & h)
    : PacketSocket(h)
{
}

BobHandler & BobServerSocket::getBobHandler() const
{
    return static_cast<BobHandler &>(MasterHandler());
}

void BobServerSocket::onPubKeyReqPacket(const PubKeyReqPacket & packet)
{
    send(PubKeyPacket(getBobHandler().conf.getPubKey()));
}

void BobServerSocket::onMsgPacket(const MsgPacket & packet)
{
    try
    {
        Message msg(packet.msg);

        msg.decryptCek(getBobHandler().conf.getKey(),
            getBobHandler().conf.getPubParam());
        msg.decryptMsg();
        std::cout << "Message" << std::endl
            << Utils::ident(Utils::octetsToStr(msg.msg));
    }
    catch (ClException e)
    {
        e.print();
        std::cout << std::endl;
    }
}
