/*
 * Title:
 *     Bob Client Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Used by Bob to setup key-pair
 *
 * NOTE:
 *     <note>
 */

#ifndef _BOBCLIENTSOCKET_H_
#define _BOBCLIENTSOCKET_H_

#include <iostream>
#include <string>
#include "common/clalgorithms.h"
#include "common/sockets/sslclientsocket.h"
#include "bob/bobconfig.h"

class BobClientSocket
    : public SslClientSocket
{
protected:

    BobConfig & m_bobConfig;

public:

    BobClientSocket(ISocketHandler & h, BobConfig & bc);

    void OnConnect();

    void OnConnectFailed();

    void onKeyPacket(const KeyPacket & packet) throw(ClException);

    void onInfoPacket(const InfoPacket & packet);
};

#endif // _BOBCLIENTSOCKET_H_
