/*
 * Title:
 *     Bob Configuration
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Configuration of Bob
 *
 * NOTE:
 *     <note>
 */

#ifndef _BOBCONFIG_H_
#define _BOBCONFIG_H_

#include "common/serverconfig.h"
#include "common/clientconfig.h"
#include "common/pubparam.h"
#include "common/key.h"
#include "common/pubkey.h"
#include "common/clexception.h"

namespace
{
    const std::string bobConf    = ".bobconfig";
    const std::string clientConf = ".bobclientconfig";
    const std::string bobPpConf  = ".bobppconfig";
    const std::string bobQidConf = ".bobqidconfig";
    const std::string bobPkConf  = ".bobpkconfig";
}

class BobConfig
{
public:

    /// PPS connection
    ClientConfig cc;

    // Bob server
    port_t port;

    /// KGC login & pass
    std::string  kgcLogin;
    std::string  kgcPass;

private:

    PubParam * m_pp;
    Key      * m_qId;
    PubKey   * m_pk;

public:

    BobConfig();

    ~BobConfig();

    /// Loads configuration from file
    void load() throw(ClException);

    /// Saves configuration
    void save() throw(ClException);

    PubParam & getPubParam() const throw(ClException);

    void setPubParam(const PubParam & pp);

    Key & getKey() const throw(ClException);

    void setKey(const Key & k);

    PubKey & getPubKey() const throw(ClException);

    void setPubKey(const PubKey & pk);

    friend std::ostream & operator<<(std::ostream & os, const BobConfig & c);
};

#endif  // _BOBCONFIG_H_
