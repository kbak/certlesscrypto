/*
 * Title:
 *     Bob Socket Handler
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Handles Bob sockets
 *
 * NOTE:
 *     <note>
 */

#ifndef _BOBHANDLER_H_
#define _BOBHANDLER_H_

#include "common/sockets/packethandler.h"
#include "bob/bobconfig.h"

class BobHandler
    : public PacketHandler
{
public:

    BobConfig & conf;

public:

    BobHandler(BobConfig & c)
        : PacketHandler(c.getPubParam())
        , conf(c)
     {
     }
};

#endif  // _BOBHANDLER_H_
