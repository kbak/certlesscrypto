/*
 * Title:
 *     CL-PKE Algorithms
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Basic algorithms required by CL-PKE scheme
 */

#include "common/clalgorithms.h"

SystemParam ClAlgorithms::setup(const size_t & n) throw(ClException)
{
    try
    {
        return SystemParam(n);
    }
    catch (ClException e)
    {
        throw ClException("ClAlgorithms::setup:\n" + e.toString());
    }
}

Key ClAlgorithms::partialPrivateKeyExtract(const SystemParam & sp,
   const std::string & id)
{
    element_t qId;
    derivePub(qId, sp.pp, id);

    Key pk(qId, sp.s);
    element_clear(qId);
    return pk;
}

void ClAlgorithms::setSecretValue(element_t & x, const PubParam & pp)
{
    element_init_Zr(x, const_cast<PubParam &>(pp).pairing);
    element_random(x);
}

Key ClAlgorithms::setPrivateKey(const Key & dId, const element_t & x)
{
    return Key(dId.key, x);
}

PubKey ClAlgorithms::setPublicKey(const element_t & x, const PubParam & pp)
{
    return PubKey(x, pp);
}

Ciphertext ClAlgorithms::encrypt(const Octets & m, const PubParam & pp,
    const PubKey & pk, const std::string & id) throw(ClException)
{
    element_t qId;
    element_t l;
    element_t theta;

    // 1. e(pk.xPub, pp.pPub) == e(pk.yPub, pp.p) ?
    if (!isPubKeyOk(pp, pk))
    {
        throw ClException("ClAlgorithms::encrypt: Invalid public key");
    }

    // 2. qId = H1(id)
    derivePub(qId, pp, id);

    // 3. random rho
    Octets rho(Utils::generate(pp.getHashLen()));

    // 4. r = H3(rho, m)
    element_init_Zr(l, const_cast<PubParam &>(pp).pairing);
    HashFun::rhoDataToRange(l, rho, m, pp.h);

    // 5. compute ciphertext
    element_init_GT(theta, const_cast<PubParam &>(pp).pairing);
    pairing_apply(theta, const_cast<PubKey &>(pk).yPub, qId,
        const_cast<PubParam &>(pp).pairing);
    element_pow_zn(theta, theta, l);

    Ciphertext c(Key(pp.p, l),
        Utils::xorOct(rho,
            pp.h(HashFun::pointToOctets(theta, pp.cp.getPBitLen()))),
        Utils::xorOct(m, HashFun::hashBytes(m.size(), rho, pp.h)));

    element_clear(theta);
    element_clear(l);
    element_clear(qId);
    return c;
}

Octets ClAlgorithms::decrypt(const Ciphertext & c, const Key & pk,
    const PubParam & pp) throw(ClException)
{
    element_t theta;
    element_t l;

    // 1. rho
    element_init_GT(theta, const_cast<PubParam &>(pp).pairing);
    pairing_apply(theta, const_cast<Ciphertext &>(c).u.key,
        const_cast<Key &>(pk).key, const_cast<PubParam &>(pp).pairing);
    Octets rho(Utils::xorOct(
        c.v, pp.h(HashFun::pointToOctets(theta, pp.cp.getPBitLen()))));
    element_clear(theta);

    // 2. m
    Octets m(Utils::xorOct(c.w, HashFun::hashBytes(c.w.size(), rho, pp.h)));

    // 3. r = H3(rho, m)
    element_init_Zr(l, const_cast<PubParam &>(pp).pairing);
    HashFun::rhoDataToRange(l, rho, m, pp.h);

    Key u(pp.p, l);
    element_clear(l);

    if (0 != element_cmp(u.key, const_cast<Ciphertext &>(c).u.key))
    {
        throw ClException("ClAlgorithms::decrypt: Decryption error");
    }
    return Octets(m);
}

bool ClAlgorithms::isPubKeyOk(const PubParam & pp, const PubKey & pk)
{
    element_t e1;
    element_t e2;
    bool      isOk;
    PubParam & rPp = const_cast<PubParam &>(pp);

    element_init_GT(e1, rPp.pairing);
    element_init_GT(e2, rPp.pairing);
    pairing_apply(e1, rPp.pPub, const_cast<PubKey &>(pk).xPub, rPp.pairing);
    pairing_apply(e2, rPp.p, const_cast<PubKey &>(pk).yPub, rPp.pairing);
    isOk = 0 == element_cmp(e1, e2);
    element_clear(e1);
    element_clear(e2);
    return isOk;
}


void ClAlgorithms::derivePub(element_t qId, const PubParam & pp,
    const std::string & id)
{
    element_init_G2(qId, const_cast<PubParam &>(pp).pairing);
    HashFun::hashToPoint(qId, Utils::strToOctets(id), pp.h);
}
