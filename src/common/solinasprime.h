/*
 * Title:
 *     Solinas Prime
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Representation and generation of Solinas primes
 *
 * NOTE:
 *     The following relation holds:
 *     exp2, exp1, sign1, sign0:
 *	     q = 2^exp2 + sign1 * 2^exp1 + sign0 * 1 (Solinas prime)
 *       where signX is  1 when isNegX is false
 *             signX is -1 when isNegX is true
 */

#ifndef _SOLINASPRIME_H_
#define _SOLINASPRIME_H_

#include <string>
#include <iostream>
#include <sstream>
#include <gmp.h>
#include "common/utils.h"

namespace
{
    /// Default Solinas Prime
    const unsigned long defExp2 = 140UL;
	const unsigned long defExp1 = 48UL;
	const bool defIsNeg1 = true;
	const bool defIsNeg0 = true;
}

/// Class storing solinas prime
class SolinasPrime
{
public:

    unsigned long exp2;
	unsigned long exp1;
	bool isNeg1;
	bool isNeg0;

public:

    /// Default Solinas prime
    SolinasPrime();

    /// Set Solinas prime
    SolinasPrime(const unsigned long & a_exp2,
        const unsigned long & a_exp1,
        const bool a_isNeg1,
        const bool a_isNeg0);

    /// Returns parameters in format accepted by PBC
    std::string getParams() const;

    /// Converts number to string and returns it
    std::string getPrime() const;

    /// Converts exp2 to string and returns it
    std::string getExp2() const;

    /// Converts exp1 to string and returns it
    std::string getExp1() const;

    /// Converts isNeg1 to string and returns it
    std::string getSign1() const;

    /// Converts isNeg0 to string and returns it
    std::string getSign0() const;

    /// Computes value of p
    void compute(mpz_t p) const;

    friend
    std::ostream & operator<<(std::ostream & os, const SolinasPrime & q);

protected:

    /// Converts exp to string
    static std::string getExp(const unsigned long & exp);

    /// Converts isNeg to signed integer
    static std::string getSign(const bool isNeg);

    /**
     * Counts q += sign * 2^exp where sign = 1 if isNeg is false and
     * sign = -1 if isNeg is true
     */
    static
    void addSigned2exp(mpz_t p, const bool isNeg, const unsigned long & exp);
};

#endif  // _SOLINASPRIME_H_
