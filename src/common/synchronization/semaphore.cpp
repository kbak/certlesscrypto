/*
 * Title:
 *     Semaphore
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     POSIX/Windows Semaphore wrapper
 */

#include "common/synchronization/semaphore.h"

Semaphore::Semaphore(int value) throw(ThreadException)
{
    bool isFailed = false;

#ifdef _WIN32
    m_sem = CreateSemaphore(NULL, value, 1, NULL);
    isFailed = NULL == m_sem;
#else
    isFailed = 0 != sem_init(&m_sem, 0, value);
#endif
    if (isFailed) throw ThreadException("Semaphore init error");
}


Semaphore::~Semaphore()
{ 
#ifdef _WIN32
    CloseHandle(m_sem);
#else
    sem_destroy(&m_sem); 
#endif
}

void Semaphore::p() throw(ThreadException)
{
    bool isFailed = false;

#ifdef _WIN32
    isFailed = WAIT_FAILED == WaitForSingleObject(m_sem, INFINITE);
#else
    isFailed = 0 != sem_wait(&m_sem);
#endif
    if (isFailed) throw ThreadException("Semaphore p error");
}

void Semaphore::v() throw(ThreadException)
{
    bool isFailed = false;

#ifdef _WIN32
    isFailed = 0 == ReleaseSemaphore(m_sem, 1, NULL);
#else
    isFailed = 0 != sem_post(&m_sem);
#endif
    if (isFailed) throw ThreadException("Semaphore v error");
}
  
