/*
 * Title:
 *     Condition Variable
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Monitor's condition
 */

#include "common/synchronization/condition.h"

Condition::Condition() throw(ThreadException)
    : w(0)
    , waitingCount(0)
{
}

void Condition::wait() throw(ThreadException)
{
    w.p();
}

bool Condition::signal() throw(ThreadException)
{
    if (0 < waitingCount)
    {
	    --waitingCount;
		w.v();
		return true;
    }
	else
	    return false;
}
