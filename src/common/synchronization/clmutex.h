/*
 * Title:
 *     Mutex
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     POSIX/Windows Mutex wrapper
 *
 * NOTE:
 *     <note>
 */

#ifndef _CLMUTEX_H_
#define _CLMUTEX_H_

#include <string>
#ifdef _WIN32
#include <windows.h>
typedef HANDLE pthread_mutex_t;
#else
#include <pthread.h>
#endif
#include "common/synchronization/threadexception.h"

/// Class representing mutex
class ClMutex
{
public:

    pthread_mutex_t m_mutex;

public:

    /// Mutex initialization
    ClMutex() throw(ThreadException);

    /// Mutex destruction
    ~ClMutex();

    /// Lock function (P)
    void lock() throw(ThreadException);

    /// Unlock function (V)
    void unlock();
};

#endif  // _CLMUTEX_H_
