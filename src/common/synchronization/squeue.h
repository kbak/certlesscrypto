/*
 * Title:
 *     Synchronized queue
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Synchronized FIFO queue
 *
 * NOTE:
 *     <note>
 */

#ifndef _SQUEUE_H_
#define _SQUEUE_H_

#include <queue>
#include "common/synchronization/condition.h"
#include "common/synchronization/monitor.h"
#include "common/synchronization/threadexception.h"

/// Synchronized FIFO queue
template <typename T>
class SQueue
{
private:

    Condition m_empty;
    Monitor   m_mon;

    std::queue<T> m_queue;

public:

    /// Pops the first element from queue
    T poll() throw(ThreadException)
    {
        m_mon.enter();
        if (0 == m_queue.size())
        {
            m_mon.wait(m_empty);
        }
        T element = m_queue.front();
        m_queue.pop();
        m_mon.leave();
        return element;
    }

    /// Pushes the element to the end of the queue
    void put(const T & element) throw(ThreadException)
    {
        m_mon.enter();
        m_queue.push(element);
        m_mon.signal(m_empty);
        m_mon.leave();
    }
};

#endif  // _SQUEUE_H_
