/*
 * Title:
 *     Thread exception
 *
 * Author:
 *     Kacper Bak
 *
 * Description:
 *     Indicates some error with threads/synchronization
 *
 * NOTE:
 *     <none>
 */

#ifndef _THREADEXCEPTION_H_
#define _THREADEXCEPTION_H_

#include "common/clexception.h"

class ThreadException
    : public ClException
{
public:

    ThreadException(const std::string & error) : ClException(error)
    {
    }
};

#endif // _THREADEXCEPTION_H_
