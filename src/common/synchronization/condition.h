/*
 * Title:
 *     Condition Variable
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Monitor's condition
 *
 * NOTE:
 *     <note>
 */

#ifndef _CONDITION_H_
#define _CONDITION_H_

#include "common/synchronization/semaphore.h"
#include "common/synchronization/threadexception.h"

/// Class with monitor's condition
class Condition
{
    friend class Monitor;

private:

    Semaphore w;

    /// Number of waiting threads
    int waitingCount;

public:

	Condition() throw(ThreadException);

    /// Waits on signal
	void wait() throw(ThreadException);

    /// Sends signal
	bool signal() throw(ThreadException);
};

#endif  // _CONDITION_H_
