/*
 * Title:
 *     Monitor
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Monitor class
 *
 * NOTE:
 *     <note>
 */

#ifndef _MONITOR_H_
#define _MONITOR_H_

#include <string>
#include "common/synchronization/semaphore.h"
#include "common/synchronization/condition.h"
#include "common/synchronization/threadexception.h"

/// Class of monitor
class Monitor
{
private:

	Semaphore s;

public:

    Monitor() throw(ThreadException);

    /// Enters the monitor
	void enter() throw(ThreadException);

    /// Leaves the monitor
	void leave() throw(ThreadException);

    /// Wait - condition variable
	void wait(Condition & cond) throw(ThreadException);

    /// Signal - condition variable
	void signal(Condition & cond) throw(ThreadException);
};

#endif  // _MONITOR_H_
