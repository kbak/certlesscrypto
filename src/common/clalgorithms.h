/*
 * Title:
 *     CL-PKE Algorithms
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Basic algorithms required by CL-PKE scheme
 *
 * NOTE:
 *     Algorithms work on EC y^2 = x^3 + x which is supported by PBC (type a)
 */

#ifndef _CLALGORITHMS_H_
#define _CLALGORITHMS_H_

#include <string>
#include "common/systemparam.h"
#include "common/hashfun.h"
#include "common/key.h"
#include "common/pubkey.h"
#include "common/ciphertext.h"
#include "common/clexception.h"

/// Class containing CL-PKE functions
class ClAlgorithms
{
public:

    /**
     * Generates System parameters corresponding to given security parameter
     * (RFC5091 5.1.2)
     */
    static SystemParam setup(const size_t & n) throw(ClException);

    /// Computes qId = H1(id), dId = sqId
    static Key partialPrivateKeyExtract(const SystemParam & sp,
        const std::string & id);

    /// Chooses random x belonging to Zq*
    static void setSecretValue(element_t & x, const PubParam & pp);

    /// Computes sId = xId
    static Key setPrivateKey(const Key & dId, const element_t & x);

    /// Computes (xPub, yPub) = (xp, xpPub)
    static PubKey setPublicKey(const element_t & x, const PubParam & pp);

    /**
     * Encryption
     * e(xPub, pPub) = e(yPub, p) ?
     * qId = H1(id)
     * random rho
     * l = H3(rho, m)
     * c = (u, v, w) = (lp, rho xor H2(e(qId, yPub)^l), m xor H4(rho))
     */
    static Ciphertext encrypt(const Octets & m, const PubParam & pp,
        const PubKey & pk, const std::string & id) throw(ClException);

    /**
     * Decryption
     * rho = v xor H2(e(sId, u))
     * m = w xor H4(rho)
     * r = H3(rho, m)
     * u = rp ?
     */
    static
    Octets decrypt(const Ciphertext & c, const Key & pk, const PubParam & pp)
        throw(ClException);

    /// Validates public key e(xPub, pPub) = e(yPub, p) ?
    static bool isPubKeyOk(const PubParam & pp, const PubKey & pk);

    /// Computes qId = H1(id)
    static
    void derivePub(element_t qId, const PubParam & pp, const std::string & id);
};

#endif  // _CLALGORITHMS_H_
