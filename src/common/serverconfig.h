/*
 * Title:
 *     Server Configuration
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     SSL Server basic configuration
 *
 * NOTE:
 *     <note>
 */

#ifndef _SERVERCONFIG_H_
#define _SERVERCONFIG_H_

#include <string>
#include <iostream>
#include <fstream>
#include <socket_include.h>
#include "common/utils.h"

class ServerConfig
{
public:

    std::string filename;
    size_t nThreads;
    port_t port;
    std::string certFile;
    std::string certPass;

public:

    ServerConfig(const std::string & a_filename);

    /// Loads configuration from file
    void load() throw(ClException);

    /// Saves configuration
    void save();

    friend
    std::ostream & operator<<(std::ostream & os, const ServerConfig & sc);

};

#endif  // _SERVERCONFIG_H_
