/*
 * Title:
 *     Generic exception
 *
 * Author:
 *     Kacper Bak
 *
 * Description:
 *     Indicates some error
 *
 * NOTE:
 *     <none>
 */

#ifndef _CLEXCEPTION_H_
#define _CLEXCEPTION_H_

#include <exception>
#include <string>
#include <iostream>

class ClException
    : public std::exception
{
protected:

    std::string m_error;

public:

    ClException(const std::string & error) : m_error(error)
    {
    }

    ~ClException() throw()
    {
    }

    std::string toString() const
    {
        return m_error;
    }

    void print() const
    {
        std::cout << m_error;
    }
};

#endif // _CLEXCEPTION_H_
