/*
 * Title:
 *     Elliptic Curve Parameters
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Parameters describing elliptic curve
 *
 * NOTE:
 *     The following relation holds:
 *     p, h:
 *       q * h = p + 1
 *       p is a prime, h is a multiple of 12 (thus p = -1 mod 12)
 *
 *     In PBC library q is name for p, and r is name for q, h is name for r.
 */

#ifndef _CURVEPARAM_H_
#define _CURVEPARAM_H_

#include <string>
#include <iostream>
#include <gmp.h>
#include <pbc.h>
#include "common/solinasprime.h"
#include "common/clexception.h"
#include "common/utils.h"

///Class storing Elliptic Curve Public Parameters.
class CurveParam
{
public:

    /// q is Solinas prime
    SolinasPrime q;

    /// Parameter of field F_p
    mpz_t p;

private:

    /// String representation of p
    std::string m_pStr;

    /// Bit-length of p
    size_t m_pBitLen;

    /// String representation of r
    std::string m_rStr;

    /// String representation of Curve Parameters
    std::string m_cpStr;

    /// Structure used by parameter generator
    a_param_t m_aParam;

public:

    CurveParam();

    /// Generates new parameters of given bit-lengths
    CurveParam(const size_t & n_p, const size_t & n_q);

    /// Sets Curve parameters
	CurveParam(const std::string & pStr, const SolinasPrime & sp);

    CurveParam(const CurveParam & cp);

    ~CurveParam();

    CurveParam & operator=(const CurveParam & cp);

    /// Returns parameters in format required by PBC (Type a curve)
    const std::string & getParams() const;

    /// Returns value of p converted to string
    const std::string & getP() const;

    /// Returns bit-length of p
    size_t getPBitLen() const;

    /// Sets value of p from string
    void setP(const std::string & pStr, const size_t pBitLen = 0);

    /// Returns value of r converted to string
    const std::string & getR() const;

    /// Computes r according to formula (p + 1) / q
    void computeR() throw(ClException);

    friend
    std::ostream & operator<<(std::ostream & os, const CurveParam & cp);

    void updateParams();

protected:

    /// Generates curve parameters
    void generate(const size_t & n_p, const size_t & n_q);
};

#endif  // _CURVEPARAM_H_
