/*
 * Title:
 *     Message
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Message encrypted with CEK (Content Encryption Key)
 */

#include "common/message.h"

Message::Message()
{
}

Message::Message(const std::string & a_msg, const std::string & a_id,
    const Octets & a_cek, const std::string & a_addr,
    const port_t & a_port)
    : msg(Utils::strToOctets(a_msg))
    , id(a_id)
    , cek(a_cek)
    , addr(a_addr)
    , port(a_port)
{
}

Message::Message(const std::string & str)
{
    std::stringstream ss(str);
    std::string u;
    std::string v;
    std::string w;
    std::string msgStr;

    ss >> id >> u >> v >> w >> addr >> port >> msgStr;
    msg = Utils::strToOctets(msgStr);
    cek = Utils::strToOctets(u + "\n" + v + "\n" + w);
}

void Message::genCek()
{
    cek = Utils::generate(keyLength + blockSize);
}

void Message::encryptMsg()
{
    Base64      base64;
    std::string tmp;

    base64.encode(Utils::octetsToStr(doCryptMsg(true)), tmp, false);
    msg = Utils::strToOctets(tmp);
}

void Message::decryptMsg()
{
    Base64      base64;
    std::string tmp;

    base64.decode(Utils::octetsToStr(msg), tmp);
    msg = Utils::strToOctets(tmp);
    msg = doCryptMsg(false);
}

Octets Message::doCryptMsg(const bool & doEncrypt)
{
    Octets         outbuf(EVP_MAX_BLOCK_LENGTH + msg.size());
    int            outlen;
    int            finlen;
    EVP_CIPHER_CTX ctx;

    EVP_CIPHER_CTX_init(&ctx);

    if (0 == EVP_CipherInit_ex(&ctx, EVP_aes_256_cbc(), NULL, NULL, NULL,
        static_cast<int>(doEncrypt)))
    {
        EVP_CIPHER_CTX_cleanup(&ctx);
        throw ClException("Message::doCryptMsg: cipher init error");
    }

    if (0 == EVP_CIPHER_CTX_set_key_length(&ctx, keyLength))
    {
        EVP_CIPHER_CTX_cleanup(&ctx);
        throw ClException("Message::doCryptMsg: key length error");
    }

    if (0 == EVP_CipherInit_ex(&ctx, NULL, NULL, &cek[0], &cek[keyLength],
        static_cast<int>(doEncrypt)))
    {
        EVP_CIPHER_CTX_cleanup(&ctx);
        throw ClException("Message::doCryptMsg: cipher init error");
    }

    if (0 == EVP_CipherUpdate(&ctx, &outbuf[0], &outlen, &msg[0], msg.size()))
    {
        EVP_CIPHER_CTX_cleanup(&ctx);
        throw ClException("Message::doCryptMsg: update error");
    }

    if (0 == EVP_CipherFinal_ex(&ctx, &outbuf[outlen], &finlen))
    {
        EVP_CIPHER_CTX_cleanup(&ctx);
        throw ClException("Message::doCryptMsg: final error");
    }

    EVP_CIPHER_CTX_cleanup(&ctx);
    outbuf.resize(outlen + finlen);
    return outbuf;
}

void Message::encryptCek(const PubParam & pp, const PubKey & pk)
    throw(ClException)
{
    cek = Utils::strToOctets(ClAlgorithms::encrypt(cek, pp, pk, id).toString());
}

void Message::decryptCek(const Key & pk, const PubParam & pp)
    throw (ClException)
{
    cek = ClAlgorithms::decrypt(
        Ciphertext(Utils::octetsToStr(cek), pp.pairing), pk, pp);
}
std::string Message::toString() const
{
    std::stringstream ss;

    ss << id << std::endl << Utils::octetsToStr(cek) << std::endl
       << addr << std::endl << port << std::endl
       << Utils::octetsToStr(msg) << std::endl;
    return ss.str();
}
