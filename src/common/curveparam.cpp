/*
 * Title:
 *     Elliptic Curve Parameters
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Parameters describing elliptic curve
 */

#include "common/curveparam.h"

CurveParam::CurveParam()
{
    mpz_init(p);
    setP("4085728822893645406621261088629121963348275108687921454843");
}

CurveParam::CurveParam(const size_t & n_p, const size_t & n_q)
{
    std::cout << "Generating curve  parameters...";
    generate(n_p, n_q);
    mpz_init(p);
    setP(m_pStr, n_p);
    std::cout << "done" << std::endl;
}

CurveParam::CurveParam(const std::string & pStr, const SolinasPrime & sp)
    : q(sp)
{
    mpz_init(p);
    setP(pStr);
}

CurveParam::CurveParam(const CurveParam & cp)
    : q(cp.q)
{
    mpz_init(p);
    setP(cp.m_pStr, cp.m_pBitLen);
}

CurveParam::~CurveParam()
{
    mpz_clear(p);
}

CurveParam & CurveParam::operator=(const CurveParam & cp)
{
    q  = cp.q;
    setP(cp.m_pStr, cp.m_pBitLen);
    return (*this);
}

const std::string & CurveParam::getParams() const
{
    return m_cpStr;
}

const std::string & CurveParam::getP() const
{
    return m_pStr;
}

size_t CurveParam::getPBitLen() const
{
    return m_pBitLen;
}

void CurveParam::setP(const std::string & pStr, const size_t pBitLen)
{
    if (&m_pStr != &pStr)
    {
        m_pStr = pStr;
    }
    mpz_set_str(p, m_pStr.c_str(), 0);
    computeR();
    m_pBitLen = (0 == pBitLen) ? Utils::getBitLen(p) : pBitLen;
    updateParams();
}

const std::string & CurveParam::getR() const
{
    return m_rStr;
}

void CurveParam::computeR() throw(ClException)
{
    mpz_t r;
    mpz_t qInt;

    mpz_init(r);
    mpz_init(qInt);
    q.compute(qInt);
    mpz_set(r, p);
    mpz_add_ui(r, r, 1UL);

    if (0 < mpz_divisible_p(r, qInt))
    {
        mpz_divexact(r, r, qInt);
    }
    else
    {
        mpz_clear(qInt);
        throw
        ClException("CurveParam::computeR: Incorrect r (PBC: h) parameter");
    }

    mpz_clear(qInt);
    m_rStr = Utils::mpzToStr(r, 10);
    mpz_clear(r);
}

std::ostream & operator<<(std::ostream & os, const CurveParam & cp)
{
    os << "Modulus p" << std::endl << Utils::ident(cp.m_pStr) << cp.q;
    return os;
}

void CurveParam::generate(const size_t & n_p, const size_t & n_q)
{
    a_param_init(m_aParam);
    a_param_gen(m_aParam, static_cast<int>(n_q), static_cast<int>(n_p));

    q.exp2   = static_cast<unsigned long>(m_aParam -> exp2);
    q.exp1   = static_cast<unsigned long>(m_aParam -> exp1);
    q.isNeg1 = -1 == m_aParam -> sign1;
    q.isNeg0 = -1 == m_aParam -> sign0;

    m_pStr = Utils::mpzToStr(m_aParam -> q, 10);
    a_param_clear(m_aParam);
}

void CurveParam::updateParams()
{
    m_cpStr = "type a q " + m_pStr + " h " + m_rStr + q.getParams();
}

