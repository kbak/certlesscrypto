/*
 * Title:
 *     Set Public Parameters packet
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Packet with CL-PKE/IBE public parameters
 *
 * NOTE:
 *     <note>
 */

#ifndef _PUBPARAMSETPACKET_H_
#define _PUBPARAMSETPACKET_H_
#include <sstream>
#include "common/pubparam.h"
#include "common/packets/packet.h"

class PubParamSetPacket
    : public Packet
{
public:

    PubParam    pp;
    std::string login;
    std::string pass;

public:

    PubParamSetPacket(const PubParam & pubParam,
        const std::string & a_login, const std::string & a_pass);

    Octets serialize() const;

    static
    PubParamSetPacket deserialize(const Octets & oct) throw(ClException);

    PackType getType() const;
};

#endif  // _PUBPARAMSETPACKET_H_
