/*
 * Title:
 *     Public Key Packet
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Packet with public key
 *
 * NOTE:
 *     <note>
 */

#ifndef _PUBKEYPACKET_H_
#define _PUBKEYPACKET_H_

#include <sstream>
#include "common/utils.h"
#include "common/pubkey.h"
#include "common/packets/packet.h"

class PubKeyPacket
    : public Packet
{
public:

    const PubKey pk;

public:

    PubKeyPacket(const PubKey & a_pk);

    Octets serialize() const;

    static PubKeyPacket deserialize(const Octets & oct, const PubParam & pp)
        throw(ClException);

    PackType getType() const;
};

#endif /* _PUBKEYPACKET_H_ */
