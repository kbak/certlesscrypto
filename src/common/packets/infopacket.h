/*
 * Title:
 *     Info Packet
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Packet indicating error or success
 *
 * NOTE:
 *     <note>
 */

#ifndef _INFOPACKET_H_
#define _INFOPACKET_H_
#include "common/packets/packet.h"

enum InfoCode
{
    PACKET_SUCCESS,
    PACKET_AUTH_FAILED,
    PACKET_UNRECOGNIZED,
    PACKET_INVALID,
    PACKET_NO_PUBPARAM
};

class InfoPacket
    : public Packet
{
public:

    InfoCode code;

public:

    InfoPacket(const InfoCode & c) : code(c)
    {
    }

    Octets serialize() const;

    static InfoPacket deserialize(const Octets & oct);

    PackType getType() const
    {
        return INFO_PACKET;
    }
};

#endif  // _INFOPACKET_H_
