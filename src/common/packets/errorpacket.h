/*
 * Title:
 *     Error Packet
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Base class for packets sent over the network
 *
 * NOTE:
 *     <note>
 */

#ifndef _ERRORPACKET_H_
#define _ERRORPACKET_H_
#include <string>
#include "common/utils.h"
#include "common/packets/packet.h"

class ErrorPacket
    : public Packet
{
public:

    std::string errStr;

public:

    ErrorPacket(const std::string & str) : errStr(str)
    {
    }

    Octets serialize() const
    {
        return Utils::strToOctets(errStr);
    }

    static ErrorPacket deserialize(const Octets & oct)
    {
        return ErrorPacket(Utils::octetsToStr(oct));
    }

    PackType getType() const
    {
        return ERROR_PACKET;
    }
};

#endif  // _ERRORPACKET_H_
