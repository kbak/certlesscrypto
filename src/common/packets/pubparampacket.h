/*
 * Title:
 *     Public Parameters packet
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Packet with CL-PKE/IBE public parameters
 *
 * NOTE:
 *     <note>
 */

#ifndef _PUBPARAMPACKET_H_
#define _PUBPARAMPACKET_H_
#include <sstream>
#include "common/pubparam.h"
#include "common/packets/packet.h"
#include "common/packets/packetexception.h"

class PubParamPacket
    : public Packet
{
public:

    PubParam pp;

public:

    PubParamPacket(const PubParam & pubParam);

    Octets serialize() const;

	static
    PubParamPacket deserialize(const Octets & oct) throw(ClException);

	PackType getType() const;
};

#endif  // _PUBPARAMPACKET_H_
