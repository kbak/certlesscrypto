/*
 * Title:
 *     PPS Client Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Retrieves Public Parameters from PPS server
 *
 * NOTE:
 *     <note>
 */

#ifndef _PPSCLIENTSOCKET_H_
#define _PPSCLIENTSOCKET_H_
#include <iostream>
#include "common/sockets/sslclientsocket.h"

class PpsClientSocket
    : public SslClientSocket
{
public:

    PpsClientSocket(ISocketHandler & h);

    void OnConnect();

    void OnConnectFailed();

    void onPubParamPacket(const PubParamPacket & packet);

    PacketHandler & getPacketHandler() const;
};

#endif // _PPSCLIENTSOCKET_H_
