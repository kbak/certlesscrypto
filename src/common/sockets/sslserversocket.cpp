/*
 * Title:
 *     SSL Server Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Base class for SSL server sockets
 *
 * NOTE:
 *     <note>
 */

#include "common/sockets/sslserversocket.h"

SslServerSocket::SslServerSocket(ISocketHandler & h)
    : PacketSocket(h)
{
}

void SslServerSocket::Init()
{
    EnableSSL();
}

void SslServerSocket::InitSSLServer()
{
    InitializeContext("session_id", getSslHandler().certFile,
        getSslHandler().certPass, TLSv1_method());
}

SslHandler & SslServerSocket::getSslHandler() const
{
    return static_cast<SslHandler &>(MasterHandler());
}
