/*
 * Title:
 *     SSL Client Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Base class for SSL client sockets
 *
 * NOTE:
 *     <note>
 */

#ifndef _SSLCLIENTSOCKET_H_
#define _SSLCLIENTSOCKET_H_
#include <iostream>
#include "common/sockets/packetsocket.h"

class SslClientSocket
    : public PacketSocket
{
public:

    SslClientSocket(ISocketHandler & h)
        : PacketSocket(h)
    {
        EnableSSL();
    }

    void InitSSLClient()
    {
        InitializeContext("", TLSv1_method());
    }
};

#endif // _SSLCLIENTSOCKET_H_
