/*
 * Title:
 *     Packet Socket Handler
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Base class for packet sockets handlers
 *
 * NOTE:
 *     <note>
 */

#ifndef _PACKETHANDLER_H_
#define _PACKETHANDLER_H_

#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif
#include <string>
#include <SocketHandler.h>
#include <Mutex.h>
#include "common/pubparam.h"

class PacketHandler
    : public SocketHandler
{
public:

    PubParam & pp;

public:

    PacketHandler(PubParam & a_pp)
        : SocketHandler()
        , pp(a_pp)
    {
    }

    PacketHandler(Mutex & m, PubParam & a_pp)
        : SocketHandler(m)
        , pp(a_pp)
     {
     }
};

#endif  // _PACKETHANDLER_H_
