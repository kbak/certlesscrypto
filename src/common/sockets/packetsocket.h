/*
 * Title:
 *     Packet Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Socket capable of using packets
 *
 * NOTE:
 *     <note>
 */

#ifndef _PACKETSOCKET_H_
#define _PACKETSOCKET_H_

#include <string>
#include <Utility.h>
#include <TcpSocket.h>
#include <ISocketHandler.h>
#include "common/utils.h"
#include "common/packets/packet.h"
#include "common/packets/pubparampacket.h"
#include "common/packets/pubparamreqpacket.h"
#include "common/packets/pubparamsetpacket.h"
#include "common/packets/keypacket.h"
#include "common/packets/keyreqpacket.h"
#include "common/packets/pubkeypacket.h"
#include "common/packets/pubkeyreqpacket.h"
#include "common/packets/msgpacket.h"
#include "common/packets/errorpacket.h"
#include "common/packets/infopacket.h"
#include "common/sockets/packethandler.h"

class PacketSocket
    : public TcpSocket
{
public:

    PacketSocket(ISocketHandler & h);

    PacketHandler & getPacketHandler() const;
/*
    void OnWrite ()
    {
        std::cout << "OnWrite" << std::endl;
    }

    void OnException ()
    {
        std::cout << "OnException" << std::endl;
    }

    void OnDelete ()
    {
        std::cout << "OnDelete" << std::endl;
    }

    void OnConnect ()
    {
        std::cout << "OnConnect" << std::endl;
    }

    void    OnSSLConnect ()
    {
        std::cout << "OnSSLConnect" << std::endl;
    }

    void OnAccept ()
    {
        std::cout << "OnAccept" << std::endl;
    }

    void OnSSLAccept ()
    {
        std::cout << "OnSSLAccept" << std::endl;
    }

    void OnConnectFailed ()
    {
        std::cout << "OnConnectFailed" << std::endl;
    }

    void OnReconnect()
    {
        std::cout << "OnReconnect" << std::endl;
    }

    void OnDisconnect()
    {
        std::cout << "OnDisconnect" << std::endl;
    }

    void OnTimeout()
    {
        std::cout << "OnTimeout" << std::endl;
    }

    void OnConnectTimeout ()
    {
        std::cout << "OnConnectTimeout" << std::endl;
    }
*/
    void OnRawData(const char * buf, size_t len);

    virtual void onPubParamPacket(const PubParamPacket & packet);

    virtual void onPubParamReqPacket(const PubParamReqPacket & packet);

    virtual void onPubParamSetPacket(const PubParamSetPacket & packet);

    virtual void onKeyPacket(const KeyPacket & packet);

    virtual void onKeyReqPacket(const KeyReqPacket & packet);

    virtual void onPubKeyPacket(const PubKeyPacket & packet);

    virtual void onPubKeyReqPacket(const PubKeyReqPacket & packet);

    virtual void onMsgPacket(const MsgPacket & packet);

    virtual void onErrorPacket(const ErrorPacket & packet);

    virtual void onInfoPacket(const InfoPacket & packet);

    void send(const Packet & packet);
};

#endif // _PACKETSOCKET_H_
