/*
 * Title:
 *     PPS Client Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Retrieves Public Parameters from PPS server
 */

#include "common/sockets/ppsclientsocket.h"

PpsClientSocket::PpsClientSocket(ISocketHandler & h)
    : SslClientSocket(h)
{
}

void PpsClientSocket::OnConnect()
{
    send(PubParamReqPacket());
}

void PpsClientSocket::OnConnectFailed()
{
    std::cout << "failed\n[error] Cannot connect to PPS" <<std::endl;
}

void PpsClientSocket::onPubParamPacket(const PubParamPacket & packet)
{
    getPacketHandler().pp = packet.pp;
    std::cout << "done" << std::endl;
    SetCloseAndDelete();
}

PacketHandler & PpsClientSocket::getPacketHandler() const
{
    return static_cast<PacketHandler &>(MasterHandler());
}
