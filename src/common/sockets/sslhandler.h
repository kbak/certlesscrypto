/*
 * Title:
 *     SSL Socket Handler
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Base class for ssl sockets handlers
 *
 * NOTE:
 *     <note>
 */

#ifndef _SSLHANDLER_H_
#define _SSLHANDLER_H_

#include "common/threadpool/threadpool.h"
#include "common/sockets/packethandler.h"
#include "common/serverconfig.h"

class SslHandler
    : public PacketHandler
{
public:

    ThreadPool  threadPool;
    std::string certFile;
    std::string certPass;

public:

    SslHandler(Mutex & m, PubParam & pp, const ServerConfig & sc)
        : PacketHandler(m, pp)
        , threadPool(sc.nThreads)
        , certFile(sc.certFile)
        , certPass(sc.certPass)
     {
     }
};

#endif  // _SSLHANDLER_H_
