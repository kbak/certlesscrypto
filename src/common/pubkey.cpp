/*
 * Title:
 *     Public Key
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Public key used by CL-PKE/IBE cryptosystem
 */

#include "common/pubkey.h"

PubKey::PubKey(const pairing_t & pairing)
{
    element_init_G1(xPub, const_cast<pairing_t &>(pairing));
    element_init_G1(yPub, const_cast<pairing_t &>(pairing));
}

PubKey::PubKey(const std::string & xStr, const std::string & yStr,
    const pairing_t & pairing) throw(ClException)
{
    element_init_G1(xPub, const_cast<pairing_t &>(pairing));
    element_init_G1(yPub, const_cast<pairing_t &>(pairing));
    try
    {
        Utils::strToPoint(xPub, xStr);
        Utils::strToPoint(yPub, yStr);
    }
    catch(ClException e)
    {
        element_clear(xPub);
        element_clear(yPub);
        throw ClException("PubKey::PubKey:\n" + e.toString());
    }
}

PubKey::PubKey(const element_t & x, const PubParam & pp)
{
    PubParam & rPp = const_cast<PubParam &>(pp);

    element_init_G1(xPub, rPp.pairing);
    element_init_G1(yPub, rPp.pairing);
    element_pow_zn(xPub, rPp.p, const_cast<element_t &>(x));
    element_pow_zn(yPub, rPp.pPub, const_cast<element_t &>(x));
}

PubKey::PubKey(const PubKey & pk)
{
    element_init_same_as(xPub, const_cast<PubKey&>(pk).xPub);
    element_init_same_as(yPub, const_cast<PubKey&>(pk).yPub);
    element_set(xPub, const_cast<PubKey&>(pk).xPub);
    element_set(yPub, const_cast<PubKey&>(pk).yPub);
}

PubKey::~PubKey()
{
    element_clear(xPub);
    element_clear(yPub);
}

PubKey & PubKey::operator=(const PubKey & pk)
{
    element_clear(xPub);
    element_clear(yPub);
    element_init_same_as(xPub, const_cast<PubKey &>(pk).xPub);
    element_init_same_as(yPub, const_cast<PubKey &>(pk).yPub);
    element_set(xPub, const_cast<PubKey &>(pk).xPub);
    element_set(yPub, const_cast<PubKey &>(pk).yPub);
    return(*this);
}

std::string PubKey::getXpub() const
{
    return Utils::pointToStr(xPub);
}

std::string PubKey::getYpub() const
{
    return Utils::pointToStr(yPub);
}

void PubKey::read(const std::string & str, const pairing_t & pairing)
    throw(ClException)
{
    std::stringstream ss(str);
    std::string xStr;
    std::string yStr;

    ss >> xStr >> yStr;

    try
    {
        (*this) = PubKey(xStr, yStr, pairing);
    }
    catch (ClException e)
    {
        throw ClException("PubKey::read:\n" + e.toString());
    }
}

void PubKey::load(const std::string & filename, const pairing_t & pairing)
    throw(ClException)
{
    std::ifstream input(filename.c_str());
    std::string   output;

    while (input.good())
    {
        std::string tmp;
        input >> tmp;
        output.append(tmp);
        output.append(1, '\n');
    }

    try
    {
        read(output, pairing);
    }
    catch (ClException e)
    {
        throw ClException("PubKey::load:\n" + e.toString());
    }
}

void PubKey::save(const std::string & filename)
{
    std::ofstream output(filename.c_str());

    output << getXpub() << std::endl
        << getYpub() << std::endl;
    output.close();
}

std::ostream & operator<<(std::ostream & os, const PubKey & pk)
{
    os << "Public Key" << std::endl
       << "X" << std::endl << Utils::ident(pk.getXpub())
       << "Y" << std::endl << Utils::ident(pk.getYpub());

    return os;
}
