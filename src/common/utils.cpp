/*
 * Title:
 *     Utilities
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Useful types and functions
 */

#include "common/utils.h"

Octets Utils::generate(const size_t & len) throw(ClException)
{
	Octets rndOct(len);

	if (1 != RAND_bytes(&rndOct[0], len))
    {
        throw ClException("Utils::generate: Cannot generate octets");
    }
	return rndOct;
}

Octets Utils::strToOctets(const std::string & str)
{
	Octets oct(str.size());

	for (size_t i = 0; i < str.size(); ++i)
	{
        oct[i] = static_cast<Octet>(str[i]);
    }
	return oct;
}

std::string Utils::octetsToStr(const Octets & oct)
{
    std::string str;

    str.resize(oct.size());
    for (size_t i = 0; i < oct.size(); ++i)
    {
        str[i] = oct[i];
    }
    return str;
}

Octets Utils::xorOct(const Octets & a, const Octets & b) throw(ClException)
{
    if (a.size() == b.size())
    {
        Octets ab(a.size());

        for (size_t i = 0; i < a.size(); ++i)
        {
            ab[i] = a[i] ^ b[i];
        }

        return ab;
    }
    else
    {
        throw ClException("Utils::xorOct: Octets of different size");
    }
}

std::string Utils::mpzToStr(const mpz_t n, const unsigned long & base)
{
    std::string nStr;
    size_t i;

    nStr.resize(mpz_sizeinbase(n, base) + 2);
    mpz_get_str(&nStr[0], base, n);

    for (i = nStr.size(); '\0' == nStr[i - 1]; --i)
        ;

    nStr.resize(i);
    return nStr;
}

size_t Utils::getBitLen(const mpz_t n)
{
    return mpz_sizeinbase(n, 2);
}

std::string Utils::elemToStr(const element_t & p)
{
    size_t decLen = static_cast<size_t>(ceil(maxBitLen * log10(2.0)));
    std::string s(decLen + 1, '\0');

    decLen = (size_t) element_snprint(static_cast<char *>(&s[0]), s.size(),
        const_cast<element_t &>(p));
    s.resize(decLen);
    return s;
}

std::string Utils::pointToStr(const element_t & pt)
{
    size_t decLen = static_cast<size_t>(ceil(maxBitLen * log10(2.0)));
    std::string s(2 * decLen + 5, '\0');

    decLen = (size_t) element_snprint(static_cast<char*>(&s[0]), s.size(),
        const_cast<element_t &>(pt));
    s.resize(decLen);

    size_t d = s.find_first_of(' ');
    return s.substr(0, d) + s.substr(d + 1, s.size());
}

void Utils::strToPoint(element_t pt, const std::string & str) throw(ClException)
{
    if (0 == element_set_str(pt, const_cast<char*>(str.c_str()), 10))
    {
        throw
        ClException("Utils::strToPoint: Invalid string to point conversion");
    }
}

std::pair<std::string, std::string> Utils::getPoint(const std::string & pt)
{
    std::string x;
    std::string y;
    size_t i;

    for (i = 1; i < pt.size() && ',' != pt[i]; x.push_back(pt[i++]))
        ;

    for (i += 2; i < pt.size() && ']' != pt[i]; y.push_back(pt[i++]))
        ;

    return std::pair<std::string, std::string>(x, y);
}

std::pair<std::string, std::string> Utils::parseParam(char * arg)
{
    int i = 0;
    int j = 0;
    int k = 0;

    for (; '-' == arg[i] && i < 2; ++i)
        ;

    for (j = i; '=' != arg[j] && '\0' != arg[j]; ++j)
        ;

    k = j;

    if ('\0' != arg[j])
    {
        k = j + 1;
    }

    return std::pair<std::string, std::string>(std::string(arg + i, j - i),
        std::string(arg + k));
}

Arguments Utils::parseParams(int argc, char * argv[])
{
    Arguments arguments;

    for (int i = 1; i < argc; ++i)
    {
        std::pair<std::string, std::string> argVal = parseParam(argv[i]);
        arguments[argVal.first] = argVal.second;
    }
    return arguments;
}

std::string Utils::ident(const std::string & str, size_t col)
{
    std::string output;

    for (size_t i = 0; i < str.size(); i += 79 - col)
    {
        output.append(col, ' ');
        output.append(str.substr(i, 79 - col));
        output.append(1, '\n');
    }
    return output;
}
