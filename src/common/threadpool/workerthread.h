/*
 * Title:
 *     Worker thread
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     A worker thread belonging to some pool
 *
 * NOTE:
 *     <note>
 */

#ifndef _WORKERTHREAD_H_
#define _WORKERTHREAD_H_

#include "common/synchronization/squeue.h"
#include "common/threadpool/task.h"
#include "common/threadpool/clthread.h"

/// Base class for tasks
class WorkerThread
    : public ClThread
{
private:

    /// Queue with tasks to process
    SQueue<Task *> & m_squeue;

public:

    WorkerThread(SQueue<Task *> & squeue);

protected:

    /// Thread's body
    void main() throw(ThreadException);

private:

    /// Copying constructor and assignment operator prohibited
    WorkerThread(const WorkerThread& );
    WorkerThread & operator=(const WorkerThread&)
    {
        return *this;
    }
};

#endif  // _WORKERTHREAD_H_
