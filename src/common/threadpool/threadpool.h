/*
 * Title:
 *     Thread pool
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     A pool of threads
 *
 * NOTE:
 *     <note>
 */

#ifndef _THREADPOOL_H_
#define _THREADPOOL_H_

#include <vector>
#include "common/synchronization/squeue.h"
#include "common/threadpool/task.h"
#include "common/threadpool/workerthread.h"

/// Thread pool class
class ThreadPool
{
private:

    /// Queue with tasks to process
    SQueue<Task *> m_tasks;

    /// Pool of worker threads
    std::vector<WorkerThread *> m_workers;

public:

    ThreadPool(const size_t & nThreads) throw(ThreadException);

    ~ThreadPool();

    /// Adds task to the pool
    void add(Task * task) throw(ThreadException);
};

#endif  // _THREADPOOL_H_
