/*
 * Title:
 *     Thread
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     POSIX/Windows Thread wrapper
 *
 * NOTE:
 *     <note>
 */

#ifndef _CLTHREAD_H_
#define _CLTHREAD_H_

#include "common/synchronization/clmutex.h"
#include "common/synchronization/threadexception.h"

/// Base class for creating threads
class ClThread
{
protected:

    volatile bool m_terminate;

private:

#ifdef _WIN32
    HANDLE     m_thread;
#else
    pthread_t  m_thread;
#endif

public:

    /// Creates a suspended thread which can be then released by run() method
    ClThread();

    virtual ~ClThread();

    /// Runs the thread
    void run() throw(ThreadException);

    /// Waits until this thread finishes
    void join() throw(ThreadException);

    /**
     * Set the flag indicating the thread to self-terminate (a user-code
     * dependent feature)
     */
    virtual void terminate();

protected:

    /// Main method of the thread
    virtual void main() = 0;

private:

    /// Thread's entry point
    static
#ifdef _WIN32
    DWORD WINAPI
#else
    void*
#endif
    entryPoint(void * pThread);
};

#endif  // _CLTHREAD_H_
