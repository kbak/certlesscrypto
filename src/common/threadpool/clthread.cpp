/*
 * Title:
 *     Thread
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     POSIX/Windows Thread wrapper
 */

#include "common/threadpool/clthread.h"

ClThread::ClThread()
{
    m_terminate = false;
}

ClThread::~ClThread()
{
    terminate();
}

void ClThread::run() throw(ThreadException)
{
    m_terminate   = false;
    bool isFailed = false;

#ifdef _WIN32
    m_thread = CreateThread(NULL, 0, entryPoint, static_cast<void*>(this),
        0, NULL);
    isFailed = NULL == m_thread;
#else
    isFailed = 0 != pthread_create(&m_thread, NULL, entryPoint,
        static_cast<void*>(this));
#endif
    if (isFailed) throw ThreadException("ClThread::run: Thread run error");
}

void ClThread::join() throw(ThreadException)
{
    bool isFailed = false;

#ifdef _WIN32
    isFailed = WAIT_FAILED == WaitForSingleObject(m_thread, INFINITE);
#else
    isFailed = 0 != pthread_join(m_thread, NULL);
#endif
    m_terminate = false;
    if (isFailed) throw ThreadException("ClThread::join: Thread join error");
}

void ClThread::terminate()
{
    m_terminate = true;
}

#ifdef _WIN32
DWORD WINAPI
#else
void*
#endif
ClThread::entryPoint(void * pThread)
{
    static_cast<ClThread *>(pThread) -> main();

#ifdef _WIN32
    return 0;
#else
    return NULL;
#endif
}
