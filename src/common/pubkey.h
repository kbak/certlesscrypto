/*
 * Title:
 *     Public Key
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Public key used by CL-PKE/IBE cryptosystem
 *
 * NOTE:
 *     <note>
 */

#ifndef _PUBKEY_H_
#define _PUBKEY_H_

#include "common/pubparam.h"

/// Class containing public key.
class PubKey
{
public:

    /// Public key
    element_t xPub;
    element_t yPub;

public:

    PubKey(const pairing_t & pairing);

    PubKey(const std::string & xStr, const std::string & yStr,
        const pairing_t & pairing) throw(ClException);

    PubKey(const element_t & x, const PubParam & pp);

    PubKey(const PubKey & pp);

    ~PubKey();

    PubKey & operator=(const PubKey & pk);

    std::string getXpub() const;

    std::string getYpub() const;

    void read(const std::string & str, const pairing_t & pairing)
            throw(ClException);

    void load(const std::string & filename, const pairing_t & pairing)
        throw(ClException);

    void save(const std::string & filename);

    friend std::ostream & operator<<(std::ostream & os, const PubKey & pk);
};

#endif  // _PUBKEY_H_
